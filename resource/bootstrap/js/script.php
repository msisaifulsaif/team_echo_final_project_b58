<script>


       jQuery(

           function($) {
               $('#message').fadeOut (550);
               $('#message').fadeIn (550);
               $('#message').fadeOut (550);
               $('#message').fadeIn (550);
               $('#message').fadeOut (550);
               $('#message').fadeIn (550);
               $('#message').fadeOut (550);
           }
       )
   </script>

   <script>

       function confirm_delete(){

           return confirm("Are You Sure?");

       }

   </script>


   <script>

       $('#deleteMultipleButton').click(function(){

            if(checkEmptySelection()){
                alert("Empty Selection! Please select some record(s) first")
            }
           else{
                var r = confirm("Are you sure you want to delete the selected record(s)?");

                if(r){
                    var selectionForm =   $('#selectionForm');
                    selectionForm.attr("action","delete_multiple.php");
                    selectionForm.submit();
                }
            }
       });


   </script>



<script>

    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });



</script>

<script>

    function checkEmptySelection(){

        emptySelection =true;

        $('.checkbox').each(function(){
            if(this.checked)   emptySelection = false;
        });

        return emptySelection;
    }


    $("#trashMultipleButton").click(function(){

        if(checkEmptySelection()){
            alert("Empty Selection! Please select some record(s) first")
        }else{

            $("#selectionForm").submit();

        }




   }) ;



</script>



<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block 5 of 5 end -->