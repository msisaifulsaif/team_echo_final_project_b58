<?php
/**
 * Created by PhpStorm.
 * User: msistudio
 * Date: 6/22/2017
 * Time: 10:50 AM
 */

namespace App\Classes;

use App\Model\Session;
use PDO;
use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;


class User  extends Database
{
    public  $id;
    public  $name;
    public  $address;
    public  $city;
    public  $country;
    public  $zip;
    public  $phone;
    public  $email;
    public  $password;
    public  $email_token;
    public  $trxid;
    public function setData($postArray){
        if (array_key_exists("id",$postArray)){
            $this->id=$postArray["id"];
        }
        if (array_key_exists("name",$postArray)){
            $this->name=$postArray["name"];
        }
        if (array_key_exists("address",$postArray)){
            $this->address=$postArray["address"];
        }
        if (array_key_exists("city",$postArray)){
            $this->city=$postArray["city"];
        }
        if (array_key_exists("country",$postArray)){
            $this->country=$postArray["country"];
        }
        if (array_key_exists("zip",$postArray)){
            $this->zip=$postArray["zip"];
        }
        if (array_key_exists("phone",$postArray)){
            $this->phone=$postArray["phone"];
        }
        if (array_key_exists("email",$postArray)){
            $this->email=$postArray["email"];
        }
        if (array_key_exists("password",$postArray)){
            $this->password=md5($postArray["password"]);
        }
        if (array_key_exists("trxid",$postArray)){
            $this->trxid=$postArray["trxid"];
            //Utility::dd($this->trxid);
        }
        if (array_key_exists("email_token",$postArray)){
            $this->email_token=md5($postArray["email_token"]);
        }
        //Utility::dd($this->email_token);

    }
    public function is_exist(){
        $email=$this->email;
        $sql="SELECT * FROM tbl_user WHERE email='$email'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
       return  $mailChk=$stmt->fetch();

    }
    public function store(){

        $name = $this->name;
        $address = $this->address;
        $city = $this->city;
        $country = $this->country;
        $zip = $this->zip;
        $phone = $this->phone;
        $email = $this->email;
        $password = $this->password;
        $email_token=$this->email_token;



            $sqlQuery = "INSERT INTO tbl_user (name,address,city,country,zip,phone,email,password,email_token) VALUES (?,?,?,?,?,?,?,?,?)";
            $dataArray = array($name, $address, $city, $country, $zip, $phone, $email, $password,$email_token);

            $stmt = $this->DBH->prepare($sqlQuery);


            $result = $stmt->execute($dataArray);


            if ($result) {
                Message::message("Success! Data has been inserted Successfully!");
                //return Utility::redirect($_SERVER['HTTP_REFERER']);
            } else {
                Message::message("Error! Data has not been inserted.");

            }



    }// end of store()
    public function login(){
        $email = $this->email;
        $password = $this->password;
        $sql = "SELECT * FROM tbl_user WHERE email='$email' && password='$password'";
        $stmt = $this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $cmrChk = $stmt->fetch();
        if($cmrChk->status=='disable'){
            Message::message("<span style='color: red; font-size: 18px'>your id has been disable</span>");
        }else {
            $sql = "SELECT * FROM tbl_user WHERE email='$email' && password='$password'";
            $stmt = $this->DBH->query($sql);
            $stmt->setFetchMode(PDO::FETCH_OBJ);
            $cmrChk = $stmt->fetch();
            if ($cmrChk != false) {
                Session::set("cmrlogin", true);
                Session::set("cmrId", $cmrChk->id);
                Session::set("cmrName", $cmrChk->name);
                Utility::redirect('orderdetails.php');
            } else {
                Message::message("Error! Email Or Password Not Matched.");
            }
        }

    }
    public function cmrInfo($id){
        $sql="SELECT * FROM tbl_user WHERE id=$id";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $cmrData=$stmt->fetch();
        //Utility::dd($cmrData);

    }

    public function update(){
        $name = $this->name;
        $address = $this->address;
        $city = $this->city;
        $country = $this->country;
        $zip = $this->zip;
        $phone = $this->phone;



        $sql="UPDATE tbl_user SET name=?,address=?,city=?,country=?,zip=?,phone=?  WHERE id=".$this->id;
        $dataArray = array($name, $address, $city, $country, $zip, $phone);


        $stmt = $this->DBH->prepare($sql);


        $result = $stmt->execute($dataArray);
        if($result){
            Message::message("Success! Data has been Updated Successfully!");
            Utility::redirect('profile.php');
        }
        else{
            Message::message("Error! Data has not been Updated.");
            Utility::redirect('profile.php');

        }
    }
    public function delete(){
        $sql="DELETE FROM tbl_brand WHERE id=".$this->id;
        $result=$this->DBH->exec($sql);
        if($result){
            Message::message("Success! Data has been Deleted Successfully!");
        }
        else{
            Message::message("Error! Data has not been Deleted.");

        }
    }
    public function view(){
        $sql="SELECT * FROM tbl_user WHERE id=".$this->id;
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $cmrData=$stmt->fetch();
    }
    public function viewSingle(){
        $query=" SELECT * FROM tbl_user WHERE email = '$this->email' ";
        // Utility::dd($query);
        $STH =$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }// end of view()
    public function validTokenUpdate(){
        $query="UPDATE tbl_user SET  email_token='".'Yes'."' WHERE email ='$this->email'";
        $result=$this->DBH->prepare($query);
        $result->execute();

        if($result){
            Message::message("
             <div class=\"alert alert-success\">
             <strong>Success!</strong> Email verification has been successful. Please login now!
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect('http://localhost/Team_Echo_B58_Final_Project/views/TEAM_ECHO/shop/login.php');
    }
    public function index(){
        $sql="SELECT * FROM tbl_user";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();

    }
    public function chkStatus(){
        $id=$this->id;
        //Utility::dd($id);


        $sql="SELECT * FROM tbl_user WHERE id=$id";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $cmrData=$stmt->fetch();
        if($cmrData->status=='active'){
            $sql="UPDATE `tbl_user` SET `status` = 'disable' WHERE `tbl_user`.`id` =$id ";
            $stmt = $this->DBH->prepare($sql);
            $result = $stmt->execute();
            if($result){
                Message::message("Success! Data has been Updated Successfully!");
                Utility::redirect('profile.php');
            }
            else{
                Message::message("Error! Data has not been Updated.");
                Utility::redirect('profile.php');

            }
        }else{
            $sql="UPDATE `tbl_user` SET `status` = 'active' WHERE `tbl_user`.`id` =$id ";
            $stmt = $this->DBH->prepare($sql);
            $result = $stmt->execute();
            if($result){
                Message::message("Success! Data has been Updated Successfully!");
                Utility::redirect('profile.php');
            }
            else{
                Message::message("Error! Data has not been Updated.");
                Utility::redirect('profile.php');

            }
        }
    }
    public function updateTx(){
        $name = $this->name;
        $address = $this->address;
        $city = $this->city;
        $country = $this->country;
        $zip = $this->zip;
        $phone = $this->phone;
        $trxid=$this->trxid;

       // Utility::dd($trxid);


        $sql="UPDATE tbl_user SET name=?,address=?,city=?,country=?,zip=?,phone=?,trxid=?  WHERE id=".$this->id;
        $dataArray = array($name, $address, $city, $country, $zip, $phone,$trxid);


        $stmt = $this->DBH->prepare($sql);


        $result = $stmt->execute($dataArray);
        if($result){
            Message::message("Success! Data has been Updated Successfully!");
            Utility::redirect('offline.php');
        }
        else{
            Message::message("Error! Data has not been Updated.");
            Utility::redirect('offline.php');

        }
    }

}