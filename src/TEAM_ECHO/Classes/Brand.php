<?php
namespace App\Classes;

use PDO;
use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;


class Brand  extends Database
{
    public $id;
    public $brand;
    public function setData($postArray){
        if (array_key_exists("id",$postArray)){
            $this->id=$postArray["id"];
        }
        if (array_key_exists("brand",$postArray)){
            $this->brand=$postArray["brand"];
        }
    }
    public function store(){

        $brand = $this->brand;


        $sqlQuery = "INSERT INTO `tbl_brand` (brand) VALUES (?)";
        $dataArray = array($brand) ;

        $stmt = $this->DBH->prepare($sqlQuery);


        $result = $stmt->execute($dataArray);


        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been inserted Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been inserted.</div>");

        }
    }// end of store()
    public function index(){
        $sql="SELECT * FROM tbl_brand";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function view(){
        $sql="SELECT * FROM tbl_brand WHERE id=".$this->id;
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $singleData=$stmt->fetch();

    }
    public function update(){
        $brand = $this->brand;
        $sql="UPDATE tbl_brand SET brand=?  WHERE id=".$this->id;
        $dataArray = array($brand) ;

        $stmt = $this->DBH->prepare($sql);


        $result = $stmt->execute($dataArray);
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been UPDATED Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been UPDATED.</div>");

        }
    }
    public function delete(){
        $sql="DELETE FROM tbl_brand WHERE id=".$this->id;
        $result=$this->DBH->exec($sql);
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been deleted Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been deleted.</div>");

        }
    }
}