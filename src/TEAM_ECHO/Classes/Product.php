<?php
/**
 * Created by PhpStorm.
 * User: msistudio
 * Date: 6/22/2017
 * Time: 10:50 AM
 */

namespace App\Classes;

use PDO;
use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;


class Product  extends Database
{
    public $id;
    public $productName;
    public $categoryId;
    public $brandId;
    public $description;
    public $price;
    public $image;
    public $type;
    public $search;
    public function setData($postArray){

        if (array_key_exists("id",$postArray)){
            $this->id=$postArray["id"];
        }
        if (array_key_exists("productName",$postArray)){
            $this->productName=$postArray["productName"];
        }
        if (array_key_exists("catId",$postArray)){
            $this->categoryId=$postArray["catId"];
        }
        //Utility::dd($this->categoryId);
        if (array_key_exists("brandId",$postArray)){
            $this->brandId=$postArray["brandId"];
        }
        if (array_key_exists("description",$postArray)){
            $this->description=$postArray["description"];
        }
        if (array_key_exists("price",$postArray)){
            $this->price=$postArray["price"];
        }
        if (array_key_exists("profilePicture",$postArray)){
            $this->image=$postArray["profilePicture"];
        }
        //Utility::dd($this->image);
        if (array_key_exists("type",$postArray)){
            $this->type=$postArray["type"];
        }
        if (array_key_exists("search",$postArray)){
            $this->search=$postArray["search"];
        }

    }
    public function store(){

         $productName=$this->productName;
         $categoryId=$this->categoryId;
         $brandId=$this->brandId;
         $description=$this->description;
         $price=$this->price;
         $image=$this->image;
         $type=$this->type;


        $sqlQuery = "INSERT INTO `tbl_product` (product_name,catid,brandid,description,price,image,featured) VALUES (?,?,?,?,?,?,?)";
        $dataArray = array($productName,$categoryId,$brandId,$description,$price,$image,$type) ;

        $stmt = $this->DBH->prepare($sqlQuery);


        $result = $stmt->execute($dataArray);


        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been inserted Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been inserted.</div>");

        }



    }// end of store()
    public function index(){
        $sql="SELECT  tbl_product.*,tbl_category.category,tbl_brand.brand
        FROM tbl_product 
        INNER JOIN tbl_category
        ON tbl_product.catid=tbl_category.id
        INNER JOIN tbl_brand
        ON tbl_product.brandid=tbl_brand.id
        ORDER BY tbl_product.id DESC ";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();

    }
    public function view(){
        $sql="SELECT p.*,c.category,b.brand  FROM tbl_product as p,tbl_category as c,tbl_brand as b
      WHERE p.catid=c.id AND p.brandid=b.id AND p.id=".$this->id;
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $singleData=$stmt->fetch();

    }
    public function update(){
        $productName=$this->productName;
        $categoryId=$this->categoryId;
        $brandId=$this->brandId;
        $description=$this->description;
        $price=$this->price;
        $image=$this->image;
        $type=$this->type;

        $sql="UPDATE tbl_product SET product_name=?,catid=?,brandid=?,description=?,price=?,image=?,featured=? WHERE id=".$this->id;
        $dataArray = array($productName,$categoryId,$brandId,$description,$price,$image,$type) ;

        $stmt = $this->DBH->prepare($sql);


        $result = $stmt->execute($dataArray);
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been UPDATED Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been UPDATED.</div>");

        }
    }
    public function delete(){
        $singleData=$this->view();
        $delImg= $singleData->image;
        unlink($delImg);


        $sql="DELETE FROM tbl_product WHERE id=".$this->id;
        $result=$this->DBH->exec($sql);
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been deleted Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been deleted.</div>");

        }
    }
    public function getFeaturedProduct(){
        $sql="SELECT * FROM tbl_product WHERE featured='1' ORDER BY id DESC LIMIT 4";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $featuredData=$stmt->fetchAll();

    }
    public function getNewProduct(){
        $sql="SELECT * FROM tbl_product  ORDER BY id DESC LIMIT 4";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $newProduct=$stmt->fetchAll();

    }
    public function getProductByBrand(){
        $sql="SELECT * FROM tbl_product GROUP  BY brandid ORDER BY id DESC LIMIT 3";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $newBrandProduct=$stmt->fetchAll();
    }
    public function getProductByCat(){
        $sql="SELECT * FROM tbl_product WHERE catid=".$this->categoryId;
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $catProduct=$stmt->fetchAll();
    }
    public function insertCompareData($cmrId,$compareId)
    {
        $csql="SELECT * FROM tbl_compare WHERE cmrId='$cmrId' AND productId='$compareId'";
        $stmt=$this->DBH->query($csql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$stmt->fetchAll();
        //Utility::dd($alldata);
        if ($alldata){
            Message::message("<div class='alert alert-danger'>Error! Product Already Added To Compare.</div>");
        } else {

            $sql = "SELECT * FROM tbl_product WHERE id='$compareId'";
            $stmt = $this->DBH->query($sql);
            $stmt->setFetchMode(PDO::FETCH_OBJ);
            $result = $stmt->fetch();
            if ($result) {
                $productId = $result->id;
                $productName = $result->product_name;
                $price = $result->price;
                $image = $result->image;

                $sqlQuery = "INSERT INTO tbl_compare (cmrId,productId,productName,price,image) VALUES (?,?,?,?,?)";
                $dataArray = array($cmrId, $productId, $productName, $price, $image);

                $stmt = $this->DBH->prepare($sqlQuery);
                $result = $stmt->execute($dataArray);
                if($result){
                    Message::message("<div class='alert alert-success'>Success! Data has been inserted Successfully!</div>");
                }
                else{
                    Message::message("<div class='alert alert-danger'>Error! Data has not been inserted.</div>");

                }

            }
        }

    }
    public function showComparelist($cmrId){
        $csql="SELECT * FROM tbl_compare WHERE cmrId='$cmrId'";
        $stmt=$this->DBH->query($csql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function chkCompare(){
        $sql="SELECT * FROM tbl_compare ";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function delCompareData($id){
        $sql="DELETE FROM tbl_compare WHERE cmrid=$id";
        $result=$this->DBH->exec($sql);
    }
    public function insertWlistData($id,$productId){
        $csql="SELECT * FROM tbl_wlist WHERE cmrId='$id' AND productId='$productId'";
        $stmt=$this->DBH->query($csql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$stmt->fetchAll();
        if ($alldata){
            Message::message("<div class='alert alert-danger'>Error! Product Already Added To Cart.</div>");
        } else {

            $sql = "SELECT * FROM tbl_product WHERE id='$productId'";
            $stmt = $this->DBH->query($sql);
            $stmt->setFetchMode(PDO::FETCH_OBJ);
            $result = $stmt->fetch();
            if ($result) {
                $productId = $result->id;
                $productName = $result->product_name;
                $price = $result->price;
                $image = $result->image;

                $sqlQuery = "INSERT INTO tbl_wlist (cmrId,productId,productName,price,image) VALUES (?,?,?,?,?)";
                $dataArray = array($id, $productId, $productName, $price, $image);

                $stmt = $this->DBH->prepare($sqlQuery);
                $result = $stmt->execute($dataArray);
                if($result){
                    Message::message("<div class='alert alert-success'>Success! Data has been inserted Successfully!</div>");
                }
                else{
                    Message::message("<div class='alert alert-danger'>Error! Data has not been inserted.</div>");

                }

            }
        }
    }
    public function showWlist($cmrId){
        $csql="SELECT * FROM tbl_wlist WHERE cmrId='$cmrId'";
        $stmt=$this->DBH->query($csql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function chkWlist(){
        $sql="SELECT * FROM tbl_wlist ";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function delWlistById($id,$productId){
        $sql="DELETE FROM tbl_wlist WHERE productId=$productId AND cmrId=$id";
        $result=$this->DBH->exec($sql);
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been Deleted Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been Deleted.</div>");

        }
    }
    public function productByBrand(){
        $sql="SELECT  tbl_product.*,tbl_brand.brand
        FROM tbl_product 
        INNER JOIN tbl_brand
        ON tbl_product.brandid=tbl_brand.id
        ORDER BY id DESC  ";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }


    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from tbl_product  LIMIT $start,$itemsPerPage";


        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }
    public function search()
    {
        $search=$this->search;

       // Utility::dd($search);
        $sql = "SELECT p.*,c.category,b.brand FROM tbl_product as p LEFT JOIN tbl_brand as b ON p.brandid = b.id LEFT JOIN tbl_category as c ON p.catid = c.id
              WHERE category LIKE '%" . $search['search'] . "%' or brand LIKE '%" . $search['search'] . "%' OR product_name LIKE '%" . $search['search'] . "%'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $searchData = $STH->fetchAll();
        Utility::dd($searchData);
        return $searchData;

    }
    public  function viewCount(){
        $id=$this->id;
        //Utility::dd($id);
        $sql="SELECT count FROM tbl_product where id=$id";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $count=$stmt->fetch();
        $count= $count->count+1;
       // Utility::dd($count);
        $sql="UPDATE tbl_product SET count = $count WHERE id =$id";
        $stmt = $this->DBH->prepare($sql);
        $stmt->execute();
    }

    public function getByIphone(){
        $sql="SELECT p.*,c.category,b.brand FROM tbl_product as p 
              LEFT JOIN tbl_brand as b ON p.brandid = b.id 
              LEFT JOIN tbl_category as c ON p.catid = c.id
              WHERE brand='iphone'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function getBySamsung(){
        $sql="SELECT p.*,c.category,b.brand FROM tbl_product as p 
              LEFT JOIN tbl_brand as b ON p.brandid = b.id 
              LEFT JOIN tbl_category as c ON p.catid = c.id
              WHERE brand='samsung'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function getByDell(){
        $sql="SELECT p.*,c.category,b.brand FROM tbl_product as p 
              LEFT JOIN tbl_brand as b ON p.brandid = b.id 
              LEFT JOIN tbl_category as c ON p.catid = c.id
              WHERE brand='dell'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function getDataByBrand(){
        $brandid=$this->brandId;
        $csql="SELECT * FROM tbl_product WHERE brandId=$brandid";
        $stmt=$this->DBH->query($csql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function getProductName(){
        $sql="SELECT * FROM tbl_category WHERE id=".$this->categoryId;
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $catProduct=$stmt->fetch();
    }
    public function getBrandName(){
        $sql="SELECT * FROM tbl_brand WHERE id=".$this->brandId;
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $catProduct=$stmt->fetch();
    }
    public function getAllProduct(){
        $sql="SELECT * FROM tbl_product  ORDER BY id DESC ";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $newProduct=$stmt->fetchAll();

    }
}