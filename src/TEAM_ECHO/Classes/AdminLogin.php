<?php


namespace App\Classes;

use  App\Message\Message;
use App\Model\Database;
use  App\Model\Session;

use App\Utility\Utility;
use PDO;

//Session::checkSession();

class AdminLogin extends Database{
    public $email='' ;
    public $password='' ;

    public function __construct(){
        parent::__construct();
    }

    public function setData($dataArray){
        //Utility::dd($dataArray);
        if (array_key_exists('email', $dataArray)) {
            $this->email = $dataArray['email'];
        }
        if (array_key_exists('password', $dataArray)) {
            $this->password = md5($dataArray['password']);
        }
        //Utility::dd($this->email);
    }

    public function login(){
        $email = $this->email;
        $password = $this->password;


        $sql="SELECT * FROM tbl_admin WHERE email='$email' && password='$password'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $cmrChk=$stmt->fetch();
        //Utility::dd($cmrChk);
        if ($cmrChk != false){
            Session::init();
            Session::set("adminlogin",true);
            Session::set("adminId",$cmrChk->id);
            Session::set("adminName",$cmrChk->name);

           // Utility::dd(Session::get("adminName"));
            Utility::redirect('index.php');
        }
        else{
            Message::message("Error! Email Or Password Not Matched.");
        }

    }
    public function is_registered(){
        $query = "SELECT * FROM tbl_admin WHERE  email=$this->email AND password=$this->password";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function logged_in(){
        if ((array_key_exists('email', $_SESSION)) && (!empty($_SESSION['email']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function log_out(){
        $_SESSION['email']="";
        return TRUE;
    }

}