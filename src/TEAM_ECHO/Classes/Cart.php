<?php
/**
 * Created by PhpStorm.
 * User: msistudio
 * Date: 6/22/2017
 * Time: 10:50 AM
 */

namespace App\Classes;

use PDO;
use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;


class Cart  extends Database
{
    public $cartid;
    public $id;
    public $quantity;
    public function setData($postArray){
        if (array_key_exists("cartid",$postArray)){
            $this->cartid=$postArray["cartid"];
        }
        if (array_key_exists("id",$postArray)){
            $this->id=$postArray["id"];
        }
        if (array_key_exists("quantity",$postArray)){
            $this->quantity=$postArray["quantity"];
        }
//        $dataArray=array($this->id,$this->brand);
        //Utility::dd($postArray);
    }
    public function store(){



        $quantity  = $this->quantity;
        $product_id= $this->id;
        //Utility::dd($product_id);
        $session_id= session_id();

        $sql="SELECT * FROM tbl_product WHERE id=".$this->id;
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);

        $singleData  =$stmt->fetch();
        $product_name= $singleData->product_name;
        $price       =$singleData->price;
        $image       =$singleData->image;

        $sql="SELECT * FROM tbl_cart WHERE session_id='$session_id' AND product_id='$product_id'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$stmt->fetchAll();
        //Utility::dd($alldata);
        if ($alldata){
           Message::message("Product Already Added To Cart");
        }else {

            $sqlQuery = "INSERT INTO tbl_cart (session_id,product_id,product_name,price,quantity,image) VALUES (?,?,?,?,?,?)";
            $dataArray = array($session_id, $product_id, $product_name, $price, $quantity, $image);

            $stmt = $this->DBH->prepare($sqlQuery);


            $result = $stmt->execute($dataArray);


            if ($result) {
                Utility::redirect("cart.php");
            } else {
                Message::message("Error! Data has not been inserted.");

            }

        }

    }// end of store()
    public function index(){
        $session_id= session_id();
        $sql="SELECT * FROM tbl_cart WHERE session_id='$session_id'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();

    }
//    public function view(){
//        $sql="SELECT * FROM tbl_brand WHERE id=".$this->id;
//        $stmt=$this->DBH->query($sql);
//        $stmt->setFetchMode(PDO::FETCH_OBJ);
//        return $singleData=$stmt->fetch();
//
//    }
    public function update(){
        $quantity = $this->quantity;
        $sql="UPDATE tbl_cart SET quantity=?  WHERE cartid=".$this->cartid;
        $dataArray = array($quantity) ;

        $stmt = $this->DBH->prepare($sql);


        $result = $stmt->execute($dataArray);
        if($result){
           Utility::redirect("cart.php");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been Updated.</div>");

        }
    }
    public function delete(){
        $sql="DELETE FROM tbl_cart WHERE cartid=".$this->cartid;
        $result=$this->DBH->exec($sql);
        if($result){
            Message::message("Success! Data has been Deleted Successfully!");
        }
        else{
            Message::message("Error! Data has not been Deleted.");

        }
    }
    public  function delCustCart(){
        $session_id= session_id();
        $sql="DELETE FROM tbl_cart WHERE session_id='$session_id'";
        $result=$this->DBH->exec($sql);
        if($result){
            Message::message("Success! Data has been Deleted Successfully!");
        }
        else{
            Message::message("Error! Data has not been Deleted.");

        }
    }
    public  function  orderProduct($id){
        $session_id= session_id();
        $sql="SELECT * FROM tbl_cart WHERE session_id='$session_id'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$stmt->fetchAll();
        if ($alldata){
            foreach ($alldata as $value){
                $productId=$value->product_id;
                $productName=$value->product_name;
                $quantity=$value->quantity;
                $price=$value->price*$quantity;
                $image=$value->image;

                $sqlQuery = "INSERT INTO tbl_order (cmrId,productId,productName,quantity,price,image) VALUES (?,?,?,?,?,?)";
                $dataArray = array($id, $productId, $productName, $quantity, $price, $image);

                $stmt = $this->DBH->prepare($sqlQuery);


                $result = $stmt->execute($dataArray);
            }
        }

    }
    public  function  orderConfirm($cmrId){
        $sql="SELECT * FROM tbl_order WHERE cmrId=$cmrId AND is_order='No'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $order=$stmt->fetchAll();
        foreach ($order as $value){
            $id[]=$value['productId'];
            $productName[]=$value['productName'];
            $date=$value['date'];
        }
        $id= implode(',',$id);
        $productName=implode(',',$productName);
        if (isset($id)){
            $sql="INSERT INTO tbl_confirm (productId,productName,cmrId,date) values(?,?,?,?)";
            $dataArray=array($id,$productName,$cmrId,$date);
            $stmt = $this->DBH->prepare($sql);
            $result = $stmt->execute($dataArray);
        }
    }

    public function isorderConfirm($id){
        $sql="UPDATE tbl_order SET is_order='Yes'  WHERE cmrId=? ";
        $dataArray = array($id) ;
        $stmt = $this->DBH->prepare($sql);
        $stmt->execute($dataArray);
    }

    public function payableAmount($id){
        $sql="SELECT price FROM tbl_order WHERE cmrId=$id AND date=now()";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $amount=$stmt->fetchAll();
    }
    public function orderDetails($id){
        $sql="SELECT * FROM tbl_confirm WHERE cmrId=$id ";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $order=$stmt->fetchAll();
    }
    public function cmrOrderList(){
        $sql="SELECT * FROM tbl_order ORDER BY id DESC ";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $order=$stmt->fetchAll();
    }
    public function productShift($id,$time){
        //Utility::dd($time);
        //var_dump($id,$time);
        //die();
        $sql="UPDATE tbl_confirm SET status='shifted'  WHERE cmrId=? AND date=?";
        $dataArray = array($id,$time) ;
        $stmt = $this->DBH->prepare($sql);
        $result = $stmt->execute($dataArray);
        if($result){
            Message::message("Success! Product has been Shifted Successfully!");

        }
        else{
            Message::message("Error! Product has Not been Shifted Successfully!");

        }
    }
    public function confirmOrder($id,$time){
        $sql="UPDATE tbl_confirm SET status='confirm'  WHERE cmrId=? AND date=? ";
        $dataArray = array($id,$time) ;
        $stmt = $this->DBH->prepare($sql);
       $result = $stmt->execute($dataArray);
        if($result){
            Message::message("Success! Data has been Deleted Successfully!");

        }
        else{
            Message::message("Success! Data has been Deleted Successfully!");

        }
    }
    public  function delOrder($id,$time,$price){
        $sql="DELETE FROM tbl_order  WHERE cmrId=$id AND date='$time' AND price='$price'";
        $result=$this->DBH->exec($sql);
    }
    public function orderNotification(){
        $sql="SELECT * FROM tbl_order WHERE status='pending'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $order=$stmt->fetchAll();
    }

    public function custOrderList(){
        $sql="SELECT * FROM tbl_confirm ";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $order=$stmt->fetchAll();
    }
    public function viewByMultipleData($id,$date){
        $sql="SELECT * FROM tbl_order WHERE cmrId=$id AND date='$date'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $order=$stmt->fetchAll();
        //Utility::dd($order);
    }
    public function shipingDate($id,$date){
        $sql="SELECT update_time FROM tbl_confirm WHERE cmrId=$id AND date='$date'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $shipingorder=$stmt->fetch();
        //Utility::dd($order);
    }
    public function shipingOrderById($id){
        $sql="SELECT * FROM tbl_confirm WHERE cmrId=$id";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $shipingorder=$stmt->fetchAll();
    }
}