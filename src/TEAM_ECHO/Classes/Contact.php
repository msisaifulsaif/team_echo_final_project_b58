<?php
/**
 * Created by PhpStorm.
 * User: msistudio
 * Date: 6/22/2017
 * Time: 10:50 AM
 */

namespace App\Classes;

use PDO;
use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;


class Contact  extends Database
{
    public $id;
    public $name;
    public $email;
    public $phone;
    public $subject;
    public function setData($postArray){
        if (array_key_exists("id",$postArray)){
            $this->id=$postArray["id"];
        }
        if (array_key_exists("name",$postArray)){
            $this->name=$postArray["name"];
        }
        if (array_key_exists("email",$postArray)){
            $this->email=$postArray["email"];
        }
        if (array_key_exists("phone",$postArray)){
            $this->phone=$postArray["phone"];
        }
        if (array_key_exists("subject",$postArray)){
            $this->subject=$postArray["subject"];
        }

    }
    public function store(){
        $sqlQuery = "INSERT INTO tbl_contact (name,email,phone,subject) VALUES (?,?,?,?)";
        $stmt = $this->DBH->prepare($sqlQuery);
        $dataArray = array($this->name,$this->email,$this->phone,$this->subject);
        $result = $stmt->execute($dataArray);
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been inserted Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been inserted.</div>");

        }
    }// end of store()
    public function index(){
        $sql="SELECT * FROM tbl_contact WHERE status='No'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();

    }
    public function view(){
        $sql="SELECT * FROM tbl_contact WHERE id=".$this->id;
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $singleData=$stmt->fetch();

    }
    public function is_seen(){
        $sql="UPDATE tbl_contact SET status='Yes'  WHERE id=".$this->id;
        $stmt = $this->DBH->prepare($sql);
        $result = $stmt->execute();
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been UPDATED Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been UPDATED.</div>");

        }
    }
    public function delete(){
        //Utility::dd($this->id);
        $sql="DELETE FROM tbl_contact WHERE id=".$this->id;
        $result=$this->DBH->exec($sql);
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been deleted Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been deleted.</div>");

        }
    }
    public function all_seenData(){
        $sql="SELECT * FROM tbl_contact WHERE status<>'No'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();
    }
    public function msgCount(){
        $sql="SELECT * FROM tbl_contact WHERE status='No'";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $order=$stmt->fetchAll();
    }
}