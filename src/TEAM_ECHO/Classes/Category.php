<?php
/**
 * Created by PhpStorm.
 * User: msistudio
 * Date: 6/22/2017
 * Time: 10:50 AM
 */

namespace App\Classes;

use PDO;
use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;


class Category  extends Database
{
    public $id;
    public $category;
    public function setData($postArray){
        if (array_key_exists("id",$postArray)){
            $this->id=$postArray["id"];
        }
        if (array_key_exists("category",$postArray)){
            $this->category=$postArray["category"];
        }
    }
    public function store(){

        $category = $this->category;


        $sqlQuery = "INSERT INTO `tbl_category` (category) VALUES (?)";
        $dataArray = array($category) ;

        $stmt = $this->DBH->prepare($sqlQuery);


        $result = $stmt->execute($dataArray);


        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been inserted Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been inserted.</div>");

        }


    }// end of store()
    public function index(){
        $sql="SELECT * FROM tbl_category";
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $alldata=$stmt->fetchAll();

    }
    public function view(){
        $sql="SELECT * FROM tbl_category WHERE id=".$this->id;
        $stmt=$this->DBH->query($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $singleData=$stmt->fetch();

    }
    public function update(){
        $category = $this->category;
        $sql="UPDATE tbl_category SET category=?  WHERE id=".$this->id;
        $dataArray = array($category) ;

        $stmt = $this->DBH->prepare($sql);


        $result = $stmt->execute($dataArray);
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been UPDATED Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been UPDATED.</div>");

        }
    }
    public function delete(){
        $sql="DELETE FROM tbl_category WHERE id=".$this->id;
        $result=$this->DBH->exec($sql);
        if($result){
            Message::message("<div class='alert alert-success'>Success! Data has been deleted Successfully!</div>");
        }
        else{
            Message::message("<div class='alert alert-danger'>Error! Data has not been deleted.</div>");

        }
    }
}