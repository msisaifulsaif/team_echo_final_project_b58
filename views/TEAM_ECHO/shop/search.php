<?php include_once('header.php'); ?>
<?php
require_once ('../../../vendor/autoload.php');
use App\Classes\Product;
use App\Model\Session;

$obj=new Product();

error_reporting(0);
//\App\Utility\Utility::dd($_POST);


$obj->setData($_POST);
$searchData=$obj->search();




######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);



if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;


if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;



$pages = ceil($recordCount/$itemsPerPage);
$someData = $obj->indexPaginator($page,$itemsPerPage);
$allData= $someData;


$serial = (  ($page-1) * $itemsPerPage ) +1;



if($serial<1) $serial=1;
####################### pagination code block#1 of 2 end #########################################

if (empty($allData)  ){
    \App\Utility\Utility::redirect('404.php');
}

?>


<div class="main">
    <div class="content">
    	<div class="content_top">
    		<div class="heading">
    		<h3>Search</h3>
    		</div>
    		<div class="clear"></div>
    	</div>
	      <div class="section group">
              <?php foreach ($allData as $value){?>
    <div class="grid_1_of_4 images_1_of_4">

        <a href="preview.php?id=<?php echo $value->id;?>"><img src="admin/img/<?php echo $value->image?>" height="100px" width="100px" alt="" /></a>
        <h2><?php echo $value->product_name;?> </h2>
        <p><?php

            $text = $value->description;
            if (strlen($text)>40) {
                $text=substr($text,0,40 );
                echo $text;
            }else{
                echo $text;
            }
            ?>
        </p>
        <p><span class="price">$<?php echo $value->price;?></span></p>
        <div class="button"><span><a href="preview.php?id=<?php echo $value->id;?>" class="details">Details</a></span></div>
    </div>
<?php }?>



          </div>

        <!--  ######################## pagination code block#2 of 2 start ###################################### -->
        <div align="left" class="container">
            <ul class="pagination">

                <?php

                $pageMinusOne  = $page-1;
                $pagePlusOne  = $page+1;


                if($page>$pages) App\Utility\Utility::redirect('search.php?Page=$pages');

                if($page>1)  echo "<li><a href='search.php?Page=$pageMinusOne'>" . "&laquo" . "</a></li>";


                for($i=1;$i<=$pages;$i++)
                {
                    if($i==$page) echo '<li class="active"><a href=""> '. $i . '</a></li>';
                    else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                }
                if($page<$pages) echo "<li><a href='search.php?Page=$pagePlusOne'>" . "&raquo" . "</a></li>";

                ?>

                <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                    <?php
                    if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                    else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                    if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                    else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                    if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                    else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                    if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                    else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                    if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                    else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                    if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                    else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                    ?>
                </select>
            </ul>
        </div>
        <!--  ######################## pagination code block#2 of 2 end ###################################### -->



    </div>
</div>
</div>
<?php include_once('inc/footer.php'); ?>
