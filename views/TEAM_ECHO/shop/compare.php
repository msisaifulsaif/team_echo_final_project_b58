<?php
require_once ('../../../vendor/autoload.php');
use App\Classes\Cart;
use App\Classes\Product;
use App\Model\Session;
\App\Model\Session::init();

$login=\App\Model\Session::get("cmrlogin");
if ($login==false){
    \App\Utility\Utility::redirect('login.php');
}

$id=\App\Model\Session::get('cmrId');

$obj=new Product();
$compareData=$obj->showComparelist($id);

//\App\Utility\Utility::dd($allData);
?>
<?php include_once('header.php'); ?>

<style>
    table.tblone img{height: 100px; width: 90px;}
</style>

    <div class="main">
        <div class="content">
            <div class="cartoption">
                <div class="cartpage">
                    <h2>Compare</h2>
                    <table class="tblone">
                        <tr>
                            <th >No</th>
                            <th >Product Name</th>
                            <th >Image</th>
                            <th >Price</th>
                            <th >Action</th>
                        </tr>
                        <?php

                        $serial=1;
                        foreach ($compareData as $value) {?>

                            <tr>
                                <td><?php echo $serial;?></td>
                                <td><?php echo $value->productName;?></td>
                                <td><img src="admin/img/<?php echo $value->image;?>" alt=""/></td>
                                <td>Tk. <?php echo $value->price;?></td>
                                <td><a href="preview.php?id=<?php echo $value->productId;?>">View</a></td>

                            </tr>
                            <?php $serial++; } ?>


                    </table>

                </div>
                <div class="shopping">
                    <div class="shopleft">
                        <a href="index.php"> <img src="images/shop.png" alt="" /></a>
                    </div>

                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    </div>
<?php include_once('inc/footer.php'); ?>