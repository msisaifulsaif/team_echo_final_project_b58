<?php

require_once ('../../../vendor/autoload.php');
\App\Model\Session::init();
use App\Classes\Product;
$obj=new \App\Classes\Cart();
$objPro=new Product();
$allData=$obj->index();

if (isset($_GET['cid'])) {
    $id=\App\Model\Session::get('cmrId');
    $obj->delCustCart();
    $objPro->delCompareData($id);
    \App\Model\Session::destroy();
}
?>
<!DOCTYPE HTML>
<head>
<title>Store Website</title>
    <title>Store Website</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="css/menu.css" rel="stylesheet" type="text/css" media="all"/>

    <link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap.min.css" media="screen" />

    <script src="js/jquerymain.js"></script>
    <script src="js/script.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/nav.js"></script>
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript" src="js/nav-hover.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Doppio+One' rel='stylesheet' type='text/css'>
    <script type="text/javascript">
  $(document).ready(function($){
    $('#dc_mega-menu-orange').dcMegaMenu({rowItems:'4',speed:'fast',effect:'fade'});
  });
</script>
</head>
<body>
  <div class="wrap">
		<div class="header_top" >
            <div class="logo">
                <a href="index.php"><img src="admin/img/logo.gif" height="100px" width="200px" style="background-color: #0A246A" alt="" /></a>
                <span style="background-color: #2b542c"><img src="../../../resource/images/0014.gif" height="100px;" width="100px;" ></span>
            </div>
			  <div class="header_top_right">
			    <div class="search_box">
				    <form action="search.php" method="post">
				    	<input type="text" required="" name="search" placeholder="Search product" >
                        <input type="submit" value="SEARCH">
				    </form>
			    </div>
			    <div class="shopping_cart">
					<div class="cart">
						<a href="#" title="View my shopping cart" rel="nofollow">
								<span class="cart_title">Cart</span>
								<span class="no_product">
                  <?php


                  if ($allData){
                      $total=\App\Model\Session::get("total");
                      $qty=\App\Model\Session::get("qty");
                      echo "$".$total." <span class=\"cart_title\">||Qty</span>".$qty;
                  }else{
                      echo "Empty";
                  }


                  ?>
                                </span>
							</a>
						</div>
			      </div>
                  <div class="login">
                  <?php
                  $login=\App\Model\Session::get("cmrlogin");
                  if ($login==false){ ?>
                      <a href="login.php">Login</a>
                  <?php } else {?>
                      <a href="?cid=<?php \App\Model\Session::get('cmrId');?>">Logout</a>
                  <?php } ?>

                  </div>

                  <div class="clear"></div>
	 </div>
	 <div class="clear"></div>
 </div>
<div class="menu" style="background-image: url('images/new1.jpg')">
	<ul id="dc_mega-menu-orange" class="dc_mm-orange">
	  <li><a href="index.php">Home</a></li>
	  <li><a href="topbrands.php">Top Brands</a></li>
      <?php
      $obj=new \App\Classes\Cart();
      $cartTable=$obj->index();
      if ($cartTable){?>
          <li><a href="cart.php">Cart</a></li>
          <li><a href="payment.php">Payment</a></li>
      <?php } ?>
        <?php

        $login=\App\Model\Session::get("cmrlogin");
        if ($login==true){
            $id=\App\Model\Session::get('cmrId');
            $chkOrder=$obj->orderDetails($id);
            if ($chkOrder){   ?>
                <li><a href="orderdetails.php">Order</a></li>
            <?php  }} ?>


        <?php
        $login=\App\Model\Session::get("cmrlogin");
        if ($login==true){ ?>

            <li><a href="profile.php">Profile</a></li>
        <?php

            $chkCompare=$objPro->chkCompare();
            $chkWlist=$objPro->chkWlist();

           //\App\Utility\Utility::dd($chkCompare);
            if (!empty($chkCompare)){?>
            <li><a href="compare.php">Compare</a> </li>
        <?php }if (!empty($chkWlist)){?>
            <li><a href="wishlist.php">WishList</a> </li>
          <?php   }} ?>


	  <li><a href="contact.php">Contact</a> </li>
	  <div class="clear"></div>
	</ul>
</div>