<?php include_once('header.php'); ?>
<?php
require_once ('../../../vendor/autoload.php');

use App\Classes\Product;
use App\Classes\Cart;
use App\Message\Message;
\App\Model\Session::init();
$obj=new Product();


$obj->setData($_GET);

$catProduct=$obj->getDataByBrand();
$catName=$obj->getBrandName();

if (empty($catProduct)){
    \App\Utility\Utility::redirect('404.php');
}

?>
<div class="main">
    <div class="content">
        <div class="content_top">
            <div class="heading">
                <h3>Latest from <?php echo $catName->brand;?></h3>
            </div>
            <div class="clear"></div>
        </div>
        <div class="section group">
            <?php foreach ($catProduct as $value){?>
                <div class="grid_1_of_4 images_1_of_4">

                    <a href="preview.php?id=<?php echo $value->id;?>"><img src="admin/img/<?php echo $value->image?>" height="100px" width="100px" alt="" /></a>
                    <h2><?php echo $value->product_name;?> </h2>
                    <p><?php

                        $text = $value->description;
                        if (strlen($text)>40) {
                            $text=substr($text,0,40 );
                            echo $text;
                        }else{
                            echo $text;
                        }
                        ?>
                    </p>
                    <p><span class="price">$<?php echo $value->price;?></span></p>
                    <div class="button"><span><a href="preview.php?id=<?php echo $value->id;?>" class="details">Details</a></span></div>
                </div>
            <?php }?>
        </div>



    </div>
</div>
</div>
<?php include_once('inc/footer.php'); ?>
