<?php

require_once ('../../../vendor/autoload.php');

use App\Classes\Product;
use App\Classes\Cart;
use App\Message\Message;
\App\Model\Session::init();
$obj=new Product();

if (!isset($_GET['id'])){
    \App\Utility\Utility::redirect('404.php');
}

$obj->setData($_GET);
$singleData=$obj->view();
if (isset($_GET['id'])){
    $obj->viewCount();
}
$objCart=new Cart();
$objCat=new \App\Classes\Category();
$allCatData=$objCat->index();

if ($singleData==false){
    \App\Utility\Utility::redirect('404.php');
}

$login=\App\Model\Session::get("cmrlogin");

if ($_SERVER["REQUEST_METHOD"]=="POST"){
    if ($login==false){
        \App\Utility\Utility::redirect('login.php');
    }else{
    $objCart->setData($_REQUEST);
    $objCart->store();
    $msg=Message::message();
    }
}
?>

<?php
$id=\App\Model\Session::get('cmrId');
if ($_SERVER["REQUEST_METHOD"]=="POST" && isset($_POST['compare'])){
    $productId=$_POST['productId'];

    $obj->insertCompareData($id,$productId);
    $msg=Message::message();
}
if ($_SERVER["REQUEST_METHOD"]=="POST" && isset($_POST['wlist'])){
    $productId=$_POST['productId'];

    $obj->insertWlistData($id,$productId);
    $msg=Message::message();
}
?>

<?php include_once('header.php'); ?>

 <div class="main">
     <style>
         .mybutton{width: 100px; float: left;margin-right: 50px;}
     </style>
    <div class="content">
    	<div class="section group">
				<div class="cont-desc span_1_of_2">				
					<div class="grid images_3_of_2">
						<img src="admin/img/<?php echo $singleData->image?>" alt="" />
					</div>
				<div class="desc span_3_of_2">

                    <?php if(isset($msg)) echo "<div id='message'>$msg</div>";?>

					<h2><?php echo $singleData->product_name?> </h2>
					<div class="price">

                        <?php
                        if($singleData->featured==1)
                        {
                            echo "<span style='color:'></span>";?>
                            <p>Price: <span>$<del><?php echo $singleData->price?></del>
                                    <?php

                                    echo $singleData->price-$singleData->price*0.1;

                                    ?>
										</span></p>
                        <?php	}else{?>
                            <p>Price: <span><?php echo $singleData->price?></span></p>
                        <?php }
                        ?>
                        <p>Category: <span><?php echo $singleData->category?></span></p>
						<p>Brand:<span><?php echo $singleData->brand?></span></p>
					</div>
				<div class="add-cart">
					<form action="" method="post">
						<input type="number" class="buyfield" name="quantity" value="1"/>
						<input type="submit" class="buysubmit" name="submit" value="Buy Now"/>
					</form>				
				</div>

                 <div class="add-cart">
                   <div class="mybutton">
                     <form action="" method="post">
                         <input type="hidden" class="buyfield" name="productId" value="<?php echo $singleData->id?>"/>
                         <input type="submit" class="buysubmit" name="compare" value="Add to Compare"/>
                     </form>
                   </div>
                   <div class="mybutton">
                         <form action="" method="post">
                             <input type="hidden" class="buyfield" name="productId" value="<?php echo $singleData->id?>"/>
                             <input type="submit" class="buysubmit" name="wlist" value="Add to Wishlist"/>
                         </form>
                   </div>
                 </div>



			</div>


			<div class="product-desc">
			<h2>Product Details</h2>
            <p><?php echo $singleData->description?></p>
            </div>
				
	</div>
				<div class="rightsidebar span_3_of_1">
					<h2>CATEGORIES</h2>
					<ul>
         <?php foreach ($allCatData as $cat){?>
				      <li><a href="productbycat.php?catId=<?php echo $cat->id;?>"><?php echo $cat->category;?></a></li>
         <?php } ?>
    				</ul>
    	
 				</div>
 		</div>
 	</div>
	</div>

<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>




   <?php include_once('inc/footer.php'); ?>
  