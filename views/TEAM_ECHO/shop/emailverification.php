<?php
if(!isset($_SESSION) )session_start();
require_once ('../../../vendor/autoload.php');
use App\Message;
use App\Utility;



$obj= new \App\Classes\User();

$obj->setData($_GET);
$singleUser = $obj->viewSingle();

$_GET['email_token']=$singleUser->email_token;
//Utility\Utility::dd($singleUser);

if($singleUser->email_token == $_GET['email_token']) {
    $obj->setData($_GET);
    //App\Utility\Utility::dd($_GET['email_token']);
    $obj->validTokenUpdate();
}
else{

    if($singleUser->email_token=='Yes'){
        Message\Message::message("<div class=\"alert alert-info\">
             <strong>Don't worry! </strong>This email already verified. Please login!
              </div>");


        Utility\Utility::redirect("login.php");
    }
    else{
        Message\Message::message("
             <div class=\"alert alert-info\">
             <strong>Sorry! </strong>This Token is Invalid. Please signup with a valid email!
              </div>");
        Utility\Utility::redirect("login.php");
    }
}