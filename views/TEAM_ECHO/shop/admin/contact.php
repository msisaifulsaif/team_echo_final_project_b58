
<?php
require_once ('../../../../vendor/autoload.php');
use App\Message\Message;
use App\Classes\Category;
$obj=new \App\Classes\Contact();
$allData= $obj->index();
$seenData= $obj->all_seenData();
$msg=Message::message();
//\App\Utility\Utility::dd($allData);

if (isset($_GET['id'])){
    $obj->setData($_GET);
    $obj->delete();
    $msg=Message::message();
}

?>
<?php include 'header.php';?>
<?php include 'sidebar.php';?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>View Message</h2>
        <?php if(isset($msg)) echo "<div id='message'>$msg</div>";?>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                <tr>
                    <th>SL.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Message</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr class="odd gradeX">
                    <?php
                    $serial=1;
                    foreach ($allData as $value)
                    {?>
                    <td><?php echo $serial;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->email;?></td>
                    <td><?php echo $value->phone;?></td>
                    <td>

                        <?php
                        $text= $value->subject;
                        if (strlen($text)>40) {
                            $text=substr($text,0,40 );
                            echo $text;
                        }else{
                            echo $text;
                        }

                        ?>

                    </td>
                    <td><a href="contactview.php?id=<?php echo $value->id;?>">View</a> || <a href="contactreply.php?id=<?php echo $value->id;?>" >Reply</a>|| <a href="contactseen.php?id=<?php echo $value->id;?>" >Seen</a></td>
                </tr>
                <?php $serial++; }?>

                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="grid_10">
    <div class="box round first grid">
        <h2>Seen Message</h2>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                <tr>
                    <th>SL.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Message</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr class="odd gradeX">
                    <?php
                    $serial=1;
                    foreach ($seenData as $value)
                    {?>
                    <td><?php echo $serial;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->email;?></td>
                    <td><?php echo $value->phone;?></td>
                    <td>

                        <?php
                        $text= $value->subject;
                        if (strlen($text)>40) {
                            $text=substr($text,0,40 );
                            echo $text;
                        }else{
                            echo $text;
                        }

                        ?>

                    </td>
                    <td><a href="?id=<?php echo $value->id;?>" onclick="return confirm('Are you sure to delete???')">Delete</a></td>
                </tr>
                <?php $serial++; }?>

                </tbody>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>
<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>
<?php include 'footer.php';?>

