<?php

######## PLEASE PROVIDE Your Gmail Info. -  (ALLOW LESS SECURE APP ON GMAIL SETTING ) ########

$yourGmailAddress = 'teamecho321@gmail.com';
$yourGmailPassword = '321321321';

##############################################################################################

session_start();
include_once('../../../../vendor/autoload.php');
require '../../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

use App\Classes\Contact;
use App\Message\Message;

$obj = new Contact();

$obj->setData($_GET);

$singleData=$obj->view();


?>



<!DOCTYPE html>

<head>
    <title>Email This To A Friend</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../../resource/bootstrap/js/jquery.js"></script>

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script>tinymce.init({
            selector: 'textarea',  // change this value according to your HTML

            menu: {
                table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
                tools: {title: 'Tools', items: 'spellchecker code'}

            }
        });


    </script>


</head>
<body>



<div class="col-md-10">
    <h2>Email This To A Friend</h2>
    <form  role="form" method="post" action="contactreply.php<?php if(isset($_REQUEST['id'])) echo "?id=".$_REQUEST['id']; else echo "?list=1";?>">
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="text"  name="name"  class="form-control" id="name" placeholder="Enter name of the recipient ">
            <label for="Email">Email Address:</label>
            <input type="text"  name="email"  class="form-control" id="email" value="<?php echo $singleData->email?>">
            <label for="body">Body:</label>
            <textarea   rows="8" cols="160"  name="subject" ></textarea>

        </div>

        <input class="btn-lg btn-primary" type="submit" value="Send Email">

    </form>


    <?php
    if(isset($_REQUEST['email'])&&isset($_REQUEST['subject'])) {

        date_default_timezone_set('Etc/UTC');
        $mail = new PHPMailer;

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls'; //tls
        $mail->SMTPAuth = true;
        $mail->Username = $yourGmailAddress;
        $mail->Password = $yourGmailPassword;
        $mail->setFrom($yourGmailAddress, 'Team Echo');
        $mail->addReplyTo($yourGmailAddress, 'Team Echo');
        $mail->addAddress($_REQUEST['email'], $_REQUEST['name']);
        $mail->Subject = $_REQUEST['subject'];
        $mail->AltBody = 'This is a plain-text message body';
        $mail->Body = $_REQUEST['subject'];


        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            Message::message("<strong class='alert alert-success'>Success!</strong> Email has been sent successfully.");

            ?>
            <script type="text/javascript">
                window.location.href = 'contact.php';
            </script>
            <?php


        }

    }


    ?>
</div>




</div>
</body>


</html>