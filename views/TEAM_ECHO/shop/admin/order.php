﻿<?php include 'header.php';?>
<?php include 'sidebar.php';?>
<?php
require_once ('../../../../vendor/autoload.php');
use App\Classes\Cart;
use App\Message\Message;
$obj=new Cart();
$orderList=$obj->custOrderList();
//$obj->viewOrderByMultiple();
//\App\Utility\Utility::dd($orderList);
if (isset($_GET['shiftid'])){
    $id=$_GET['shiftid'];
    $time=$_GET['time'];
    $shift=$obj->productShift($id,$time);

}
if (isset($_GET['delproid'])){
    $id=$_GET['delproid'];
    $time=$_GET['time'];
    $price=$_GET['price'];
    $delOrderId=$obj->delOrder($id,$time,$price);
}

if (!isset($_GET['id'])){
    echo "<meta http-equiv='refresh' content='0;URL=?id=teamecho?id' />";
}

?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Inbox</h2>
                <div class="block">
                    <?php //echo $msg=Message::message();?>
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>SL</th>
                            <th>Pro Id</th>
                            <th>Pro Name</th>
                            <th>Date & Time</th>
                            <th>Cmr Id</th>
                            <th>View Details</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php
                    $serial=1;
                    $sum=0;
                        foreach ($orderList as $value){
                    ?>
						<tr class="odd gradeX">
							<td><?php echo $serial;?></td>
                            <td><?php echo $value->productId;?></td>
                            <td><?php echo $value->productName;?></td>
                            <td><?php echo \App\Format\Format::formatDate($value->date) ;?></td>
                            <td><?php echo $value->cmrId;?></td>
                            <td><a href="customer.php?id=<?php echo $value->cmrId;?>&Date=<?php echo $value->date?>">View Details</a> </td>
							<?php
                            if ($value->status=='pending'){?>
                                <td><a href="?shiftid=<?php echo $value->cmrId?>&time=<?php echo $value->date?>">Shifted</a>  </td>
                            <?php }elseif($value->status=='shifted'){?>
                                <td>Pending</td>
                            <?php } else{?>
                                <td><a href="?delproid=<?php echo $value->cmrId?>&time=<?php echo $value->date?>">Remove</a></td>
                            <?php } ?>

                        </tr>
                    <?php $serial++;} ?>

					</tbody>
				</table>
               </div>
            </div>
        </div>
<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>
<?php include 'footer.php';?>