<?php
require_once ('../../../../vendor/autoload.php');




if ($_SERVER["REQUEST_METHOD"]=="POST" && isset($_POST['submit'])){
	$obj=new \App\Classes\AdminLogin();
	$obj->setData($_POST);
	$obj->login();
}


echo $msg=\App\Message\Message::message();
?>






<!DOCTYPE html>
<head>
<meta charset="utf-8">
<title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen" />
</head>
<body>
<div class="container">
	<section id="content">
		<form action="" method="post">
			<h1>Admin Login</h1>
			<div>
				<input type="text" placeholder="Username" required="" name="email"/>
			</div>
			<div>
				<input type="password" placeholder="Password" required="" name="password"/>
			</div>
			<div>
				<input type="submit" name="submit" value="Log in" />
			</div>
		</form><!-- form -->
		<div class="button">
			<a href="#">Training with live project</a>
		</div><!-- button -->
	</section><!-- content -->
</div><!-- container -->
</body>
</html>