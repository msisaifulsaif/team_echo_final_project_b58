<?php
require_once ('../../../../vendor/autoload.php');
use App\Message\Message;
use App\Classes\Brand;
$obj=new \App\Classes\Contact();
$obj->setData($_GET);
$singleData= $obj->view();

if ($_SERVER["REQUEST_METHOD"]=="POST"){
    \App\Utility\Utility::redirect('contact.php');
}

?>
<?php include 'header.php';?>
<?php include 'sidebar.php';?>


    <div class="grid_10">
        <div class="box round first grid">
            <h2>Add New Brand</h2>

            <?php  if(isset($msg)) echo "<div id='message'>$msg</div>";?>
            <div class="block copyblock">
                <form action="" method="post">
                    <table class="form">
                        <tr>
                            <td>Name:</td>
                            <td>
                                <input type="text" name="brand" value="<?php echo $singleData->name;?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>
                                <input type="text" name="brand" value="<?php echo $singleData->email;?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>Phone:</td>
                            <td>
                                <input type="text" name="brand" value="<?php echo $singleData->phone;?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>Message:</td>
                            <td>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="comment"> <?php echo $singleData->subject;?></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>



                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Ok" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>

<?php include 'footer.php';?>