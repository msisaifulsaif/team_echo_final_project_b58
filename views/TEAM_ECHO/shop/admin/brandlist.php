﻿<?php
require_once ('../../../../vendor/autoload.php');
use App\Message\Message;
use App\Classes\Brand;
$obj=new Brand();
$allData= $obj->index();
$msg=Message::message();
?>
<?php include 'header.php';?>
<?php include 'sidebar.php';?>

        <div class="grid_10">
            <div class="box round first grid">
                <h2>Brand List</h2>
                <?php if(isset($msg)) echo "<div id='message'>$msg</div>";?>
                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Brand Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr class="odd gradeX">
							<?php
                                $serial=1;
                                foreach ($allData as $value)
                            {?>
                            <td><?php echo $serial;?></td>
							<td><?php echo $value->brand;?></td>
							<td><a href="brandedit.php?id=<?php echo $value->id;?>">Edit</a> || <a href="branddelete.php?id=<?php echo $value->id;?>" onclick="return confirm('Are You Sure To Delete?')">Delete</a></td>
						</tr>
                            <?php $serial++; }?>

					</tbody>
				</table>
               </div>
            </div>
        </div>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

<?php include 'footer.php';?>

