﻿
<?php
require_once ('../../../../vendor/autoload.php');
use App\Message\Message;
use App\Classes\Category;
$obj=new Category();
$allData= $obj->index();
$msg=Message::message();
//\App\Utility\Utility::dd($allData);

?>
<?php include 'header.php';?>
<?php include 'sidebar.php';?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Category List</h2>
                <?php if(isset($msg)) echo "<div id='message'>$msg</div>";?>
                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Category Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr class="odd gradeX">
							<?php
                                $serial=1;
                                foreach ($allData as $value)
                            {?>
                            <td><?php echo $serial;?></td>
							<td><?php echo $value->category;?></td>
							<td><a href="catedit.php?id=<?php echo $value->id;?>">Edit</a> || <a href="catdelete.php?id=<?php echo $value->id;?>" onclick="return confirm('Are You Sure To Delete?')">Delete</a></td>
						</tr>
                            <?php $serial++; }?>

					</tbody>
				</table>
               </div>
            </div>
        </div>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>
<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>
<?php include 'footer.php';?>

