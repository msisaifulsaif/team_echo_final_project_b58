﻿<?php
require_once ('../../../../vendor/autoload.php');
    use App\Message\Message;
    use App\Classes\Brand;
    $obj=new Brand();
    $obj->setData($_GET);
    $singleData= $obj->view();

    if ($_SERVER["REQUEST_METHOD"]=="POST"){
        //\App\Utility\Utility::dd($_POST);
        $obj->setData($_POST);
        $obj->update();
        $msg=Message::message();
    }



?>
<?php include 'header.php';?>
<?php include 'sidebar.php';?>


        <div class="grid_10">
            <div class="box round first grid">
                <h2>Add New Brand</h2>

                <?php  if(isset($msg)) echo "<div id='message'>$msg</div>";?>
               <div class="block copyblock"> 
                 <form action="" method="post">
                    <table class="form">					
                        <tr>
                            <td>
                                <input type="text" name="brand" value="<?php echo $singleData->brand;?>" class="medium" />
                            </td>
                            <input type="hidden" name="id" value="<?php echo $singleData->id;?>" class="medium" />
                        </tr>
						<tr> 
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>

<?php include 'footer.php';?>