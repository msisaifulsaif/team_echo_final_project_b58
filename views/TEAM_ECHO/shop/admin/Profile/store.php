<?php
if(!isset($_SESSION) )session_start();
use App\Utility\Utility;
use App\Message\Message;

require_once '../../../../../vendor/autoload.php';

$login= new \App\Classes\AdminLogin();
$status= $login->setData($_POST)->is_registered();

if($status){
    $_SESSION['email']=$_POST['email'];
    Message::message("
                <div class=\"alert alert-success\">
                            <strong>Welcome!</strong> You have successfully logged in.
                </div>");

    Utility::redirect('../index.php');

}else{
    Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Wrong information!</strong> Please try again.
                </div>");

    Utility::redirect('login.php');

}