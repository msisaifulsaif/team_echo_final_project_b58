﻿
<?php
require_once ('../../../../vendor/autoload.php');
use App\Message\Message;
use App\Classes\Product;
$obj=new Product();
$allData= $obj->index();

$msg=Message::message();
//\App\Utility\Utility::dd($allData);

?>
<?php include 'header.php';?>
<?php include 'sidebar.php';?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Post List</h2>
        <?php if(isset($msg)) echo "<div id='message'>$msg</div>";?>
        <div class="block">  
            <table class="data display datatable" id="example">
			<thead>
				<tr>
                    <th>No</th>
					<th>Title</th>
                    <th>Category</th>
                    <th>Brand</th>
					<th>Description</th>
					<th>price</th>
					<th>Image</th>
                    <th>Featured</th>
                    <th>Total View</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr class="odd gradeX">
                    <?php $serial=1;
                        foreach ($allData as $value){
                    ?>
					<td><?php echo $serial;?></td>
					<td><?php echo $value->product_name?></td>
					<td><?php echo $value->category?></td>
                    <td><?php echo $value->brand?></td>
                    <td>
                        <?php

                        $text = $value->description;
                        if (strlen($text)>40) {
                            $text=substr($text,0,40 );
                            echo $text;
                        }else{
                            echo $text;
                        }
                        ?>

                    </td>
                    <td><?php echo $value->price?></td>
                    <td><img src="img/<?php echo $value->image?>" height="100px" width="100px"> </td>
                    <td><?php echo $value->featured?></td>
                    <td><?php echo $value->count?></td>
<!--					<td class="center"> 4</td>-->
                    <td><a href="productedit.php?id=<?php echo $value->id;?>">Edit</a> || <a href="productdelete.php?id=<?php echo $value->id;?>" onclick="return confirm('Are You Sure To Delete?')">Delete</a></td>
				</tr>
				<?php $serial++;} ?>
			</tbody>
		</table>

       </div>
    </div>
</div>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
		setSidebarHeight();
    });
</script>

<?php include 'footer.php';?>
