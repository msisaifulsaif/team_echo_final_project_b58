﻿<?php
require_once ('../../../../vendor/autoload.php');
use App\Message\Message;
use App\Classes\Brand;
use App\Classes\Category;
use App\Classes\Product;
$obj=new Product();
$objBrand=new Brand();
$brandList=$objBrand->index();
$objCat=new Category();
$catList=$objCat->index();

if ($_SERVER["REQUEST_METHOD"]=="POST"){

    $fileName = time(). $_FILES['image'] ['name'];

    $source = $_FILES['image'] ['tmp_name'];

    $destination= "img/".$fileName;

    move_uploaded_file($source,$destination);

    $_POST['profilePicture'] =$fileName;

    $obj->setData($_POST);
    $obj->store();
    $msg=Message::message();
}
?>

<?php include 'header.php';?>
<?php include 'sidebar.php';?>



<div class="grid_10">
    <div class="box round first grid">
        <?php if(isset($msg)) echo "<div id='message'>$msg</div>";?>
        <h2>Add New Product</h2>
        <div class="block">               
         <form action="" method="post" enctype="multipart/form-data">
            <table class="form">
               
                <tr>
                    <td>
                        <label>Name</label>
                    </td>
                    <td>
                        <input type="text" name="productName" placeholder="Enter Product Name..." class="medium" />
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Category</label>
                    </td>
                    <td>
                        <select id="select" name="catId">
                            <option>Select Category</option>
                            <?php foreach ($catList as $value){?>
                                <option value="<?php echo $value->id;?>"><?php echo $value->category;?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Brand</label>
                    </td>
                    <td>
                        <select id="select" name="brandId">
                            <option>Select Brand</option>
                            <?php foreach ($brandList as $value){?>
                            <option value="<?php echo $value->id;?>"><?php echo $value->brand;?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
				
				 <tr>
                    <td style="vertical-align: top; padding-top: 9px;">
                        <label>Description</label>
                    </td>
                    <td>
                        <textarea class="tinymce" name="description"></textarea>
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Price</label>
                    </td>
                    <td>
                        <input type="text" name="price" placeholder="Enter Price..." class="medium" />
                    </td>
                </tr>
            
                <tr>
                    <td>
                        <label>Upload Image</label>
                    </td>
                    <td>
                        <input type="file"name="image" accept=".png, .jpg, .jpeg" />
                    </td>
                </tr>
				
				<tr>
                    <td>
                        <label>Product Type</label>
                    </td>
                    <td>
                        <select id="select" name="type">
                            <option>Select Type</option>
                            <option value="1">Featured</option>
                            <option value="2">General</option>
                        </select>
                    </td>
                </tr>

				<tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" Value="Save" />
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<!-- Load TinyMCE -->
<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>
<?php include 'footer.php';?>


