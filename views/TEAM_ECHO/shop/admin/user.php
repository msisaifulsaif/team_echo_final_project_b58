<?php
require_once ('../../../../vendor/autoload.php');
use App\Message\Message;
use App\Classes\Brand;
$obj=new \App\Classes\User();
$allData= $obj->index();
$msg=Message::message();
?>
<?php include 'header.php';?>
<?php include 'sidebar.php';?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>User List</h2>
        <?php if(isset($msg)) echo "<div id='message'>$msg</div>";?>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>Country</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr class="odd gradeX">
                    <?php
                    $serial=1;
                    foreach ($allData as $value)
                    {?>
                    <td><?php echo $serial;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->address;?></td>
                    <td><?php echo $value->city;?></td>
                    <td><?php echo $value->country;?></td>
                    <td><?php echo $value->phone;?></td>
                    <td><?php echo $value->email;?></td>
                    <td><?php echo $value->status;?></td>
                    <td>
                    <?php if ($value->status=='disable'){?>
                        <a href="userstatus.php?id=<?php echo $value->id;?>">Enable</a>
                    <?php }else{?>
                        <a href="userstatus.php?id=<?php echo $value->id;?>">Disable</a>
                    <?php }?>
                    </td>
                </tr>
                <?php $serial++; }?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

<?php include 'footer.php';?>

