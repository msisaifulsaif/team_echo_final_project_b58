<?php include 'header.php';?>
<?php include 'sidebar.php';?>
<?php
require_once ('../../../../vendor/autoload.php');
use App\Message\Message;
use App\Classes\Brand;
$obj=new \App\Classes\User();
$objCart=new \App\Classes\Cart();
$obj->setData($_GET);
$multipleData=$objCart->viewByMultipleData($_GET['id'],$_GET['Date']);
$cmrData= $obj->view();
if ($_SERVER['REQUEST_METHOD']=='POST'){
    \App\Utility\Utility::redirect('order.php');
}

$msg=Message::message();

?>

    <div class="grid_10">
        <div class="box round first grid">
            <h2>Customer Details</h2>
            <div class="block copyblock">
                <form action="" method="post">
<div class="row">
    <div class="class=col-xs-12 col-md-4">
        <table class="table table-bordered">
            <tr>
                <td colspan="3"><h2>Address</h2></td>
            </tr>
            <tr>
                <td width="20%">Name</td>
                <td width="5%">:</td>
                <td><?php echo $cmrData->name?></td>
            </tr>
            <tr>
                <td>Address</td>
                <td>:</td>
                <td><?php echo $cmrData->address?></td>
            </tr>
            <tr>
                <td>City</td>
                <td>:</td>
                <td><?php echo $cmrData->city?></td>
            </tr>
            <tr>
                <td>Country</td>
                <td>:</td>
                <td><?php echo $cmrData->country?></td>
            </tr><tr>
                <td>Zip-Code</td>
                <td>:</td>
                <td><?php echo $cmrData->zip?></td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>:</td>
                <td><?php echo $cmrData->phone?></td>
            </tr>
            <tr>
                <td colspan="3"><input type="submit" value="Ok"></td>
            </tr>
        </table>
    </div>
    <div class="class=col-xs-12 col-md-8">

        <table class="table table-bordered">

            <thead>
            <tr>
                <td colspan="8"><h2>Order List</h2></td>
            </tr>
              <tr>
                <th>Sl</th>
                <th>Product</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>price</th>
                <th>total price</th>
                <th>date</th>
                <th>image</th>
              </tr>
            </thead>

            <tbody>
                <?php
                $i=1;
                $sum=0;
                foreach ($multipleData as $value){?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $value->productId;?></td>
                    <td><?php echo $value->productName;?></td>
                    <td><?php echo $value->quantity;?></td>
                    <td><?php echo $value->price/$value->quantity;?></td>
                    <td><?php echo $value->price;?></td>
                    <td><?php echo $value->date;?></td>
                    <td><img src="img/<?php echo $value->image;?>" height="100px" width="100px"></td>

                </tr>
               <?php $i++; $sum=$sum+$value->price;}?>
            <tr>
                <td></td><td></td><td></td><td></td><td></td>
                <td colspan="6">Total=<?php echo $sum;?></td>
            </tr>
            </tbody>




        </table>

    </div>

</div>
                </form>
            </div>
        </div>
    </div>
<?php include 'footer.php';?>