<?php include_once 'header.php';?>

<?php
use App\Model\Session;
$login=\App\Model\Session::get("cmrlogin");
if ($login==false){
    \App\Utility\Utility::redirect('login.php');
}
echo $msg=\App\Message\Message::message();

?>

<style>
    .tblone{width: 550px; margin: 0px auto;border: 2px solid #ddd}
    .tblone tr td{text-align: justify}
</style>

<div class="main">
    <div class="content">
        <div class="section group">
            <?php
                $id=Session::get('cmrId');
                $objCmr=new \App\Classes\User();
                $cmrData= $objCmr->cmrInfo($id);
                //\App\Utility\Utility::dd($cmrData);
            ?>


            <table class="tblone">
                <tr>
                   <td colspan="3"><h2>Your Profile Details</h2></td>
                </tr>
                <tr>
                    <td width="20%">Name</td>
                    <td width="5%">:</td>
                    <td><?php echo $cmrData->name?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>:</td>
                    <td><?php echo $cmrData->address?></td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>:</td>
                    <td><?php echo $cmrData->city?></td>
                </tr>
                <tr>
                    <td>Country</td>
                    <td>:</td>
                    <td><?php echo $cmrData->country?></td>
                </tr><tr>
                    <td>Zip-Code</td>
                    <td>:</td>
                    <td><?php echo $cmrData->zip?></td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>:</td>
                    <td><?php echo $cmrData->phone?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="editprofile.php">Update Details</a></td>
                </tr>
            </table>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php include_once 'inc/footer.php';?>
