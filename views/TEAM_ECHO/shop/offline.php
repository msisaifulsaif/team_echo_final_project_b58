<?php include_once 'header.php';?>

<?php
$login=\App\Model\Session::get("cmrlogin");
if ($login==false){
    \App\Utility\Utility::redirect('login.php');
}
?>
<?php
use App\Classes\Cart;
use App\Model\Session;
\App\Model\Session::init();
$login=\App\Model\Session::get("cmrlogin");
if ($login==false){
    \App\Utility\Utility::redirect('login.php');
}

$obj=new \App\Classes\User();
$id=Session::get('cmrId');
$cmrData= $obj->cmrInfo($id);

if ($_SERVER["REQUEST_METHOD"]=="POST"){
    $obj->setData($_POST);
    $obj->updateTx();
    echo $msg=\App\Message\Message::message();
}



?>

<?php
$objCart=new Cart();
$allData= $objCart->index();

//if (!isset($_GET['id'])){
//    echo "<meta http-equiv='refresh' content='0;URL=?id=teamecho?id' />";
//}

?>


<?php
if (isset($_GET['orderid']) && $_GET['orderid']=='order'){
    $id=Session::get('cmrId');
    $insertOrder=$objCart->orderProduct($id);
    $objCart->orderConfirm($id);
    $objCart->isorderConfirm($id);
    $objCart->delCustCart();
    \App\Utility\Utility::redirect('success.php');
}
?>

<style>
    .division{width: 50%;float: left;}
    .tblone{width: 500px; margin: 0px auto;border: 2px solid #ddd}
    .tblone tr td{text-align: justify}
    .tblone input[type="text"]{width: 400px; padding: 5px; font-size: 15px;  }
    .tbltwo{float: right; text-align: left;width: 50%;border: 2px solid #ddd;margin-right: 14px; margin-top:12px; }
    .tbltwo tr td{text-align: justify;padding: 5px 10px;}
    .ordernow{padding-bottom: 30px;}
    .ordernow a{width: 200px; margin: 20px auto 0;text-align: center;padding: 5px; font-size: 30px;display: block;background: #FF0000;color: #fff;border-radius: 3px;}
</style>

<div class="main">
    <div class="content">
        <div class="section group">
            <div class="division">
                    <table class="tblone">
                        <h2>Your Cart</h2>
                        <?php if (isset($msg)) echo $msg;?>
                        <tr>
                            <th >No</th>
                            <th >Product</th>
                            <th >Price</th>
                            <th >Quantity</th>
                            <th >Total</th>
                        </tr>
                        <?php

                        $serial=1;
                        $sum=0;
                        $qty=0;
                        foreach ($allData as $value) {?>

                            <tr>
                                <td><?php echo $serial;?></td>
                                <td><?php echo $value->product_name;?></td>
                                <td>Tk. <?php echo $value->price;?></td>
                                <td> <?php echo $value->quantity;?></td>
                                <td><?php
                                    echo $total=$value->price*$value->quantity;

                                    ?> </td>
                                <?php
                                $sum=$sum+$total;
                                $qty=$qty+$value->quantity;
                                ?>

                            </tr>
                            <?php $serial++; } ?>


                    </table>

                    <table class="tbltwo" style="float:right;text-align:left;" width="40%">
                        <tr>
                            <th>Sub Total  </th>
                            <th>:</th>
                            <td>TK. <?php echo $sum?></td>
                        </tr>
                        <tr>
                            <th>VAT  </th>
                            <th>:</th>
                            <td>10%($<?php echo $vat=$sum*0.1?>)</td>
                        </tr>
                        <tr>
                            <th>Grand Total </th>
                            <th>:</th>
                            <td>TK.
                                <?php echo $total=$vat+$sum;
                                ?> </td>
                        </tr>
                        <tr>
                            <th>Quantity  </th>
                            <th>:</th>
                            <td><?php echo $qty?></td>
                        </tr>
                    </table>

            </div>
            <div class="division">
                <form action="" method="post">
                    <table class="tblone">
                        <tr>
                            <td colspan="2"><h2>Update Profile Details</h2></td>
                        </tr>
                        <tr>
                            <td width="20%">Name</td>
                            <td><input type="text" name="name" value="<?php echo $cmrData->name?>"></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><input type="text" name="address" value="<?php echo $cmrData->address?>"></td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td><input type="text" name="city" value="<?php echo $cmrData->city?>"></td>
                        </tr>
                        <tr>
                            <td>Country</td>
                            <td><input type="text" name="country" value="<?php echo $cmrData->country?>"></td>
                        </tr><tr>
                            <td>Zip-Code</td>
                            <td><input type="text" name="zip" value="<?php echo $cmrData->zip?>"></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><input type="text" name="phone" value="<?php echo $cmrData->phone?>"></td>
                        </tr>
                        <tr>
                            <td>Trx Id</td>
                            <td><input type="text" name="trxid" ></td>
                        </tr>
                        <input type="hidden" name="id" value="<?php echo $cmrData->id?>">
                        <tr>
                            <td></td>
                            <td><input type="submit" name="submit" value="Update"></td>
                        </tr>

                    </table>

                </form>

            </div>
        </div>
        <div class="ordernow"><a href="?orderid=order">Order</a></div>
        <div class="clear"></div>
    </div>
</div>

<?php include_once 'inc/footer.php';?>
