<?php include_once 'header.php';?>
<?php
require_once ('../../../vendor/autoload.php');
use App\Classes\Cart;
use App\Model\Session;
\App\Model\Session::init();

$login=\App\Model\Session::get("cmrlogin");
if ($login==false){
    \App\Utility\Utility::redirect('login.php');
}


$objCart=new \App\Classes\Cart();
$id=\App\Model\Session::get('cmrId');
$order=$objCart->orderDetails($id);
$shipingorder=$objCart->shipingOrderById($id);
//\App\Utility\Utility::dd($shipingorder->status=='confirm');
//\App\Utility\Utility::dd($shipingorder);
if (isset($_GET['cmrid'])){
    $id=$_GET['cmrid'];
    $time=$_GET['time'];
    $confirmOrder=$objCart->confirmOrder($id,$time);
}
if (isset($_GET['delproid'])){
    $id=$_GET['delproid'];
    $time=$_GET['time'];
    $price=$_GET['price'];
    $delOrderId=$obj->delOrder($id,$time,$price);
}

if (!isset($_GET['id'])){
    echo "<meta http-equiv='refresh' content='0;URL=?id=teamecho?id' />";
}
?>

<style>

</style>

<div class="main">
    <div class="content">
        <div class="section group">

                <h2>Your Cart</h2>
            <table class="tblone">
                <tr>
                    <th>No</th>
                    <th>Pro Name</th>
                    <th>Date</th>
                    <?php
                    foreach ($shipingorder as $status){
                    if ($status->status=='shifted'){?>
                    <th>Shiping Date</th>
                    <?php }}?>
                    <th>Details</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                <?php

                $serial=1;
                $sum=0;
                $qty=0;
                foreach ($order as $value) {?>

                    <tr>
                        <td><?php echo $serial;?></td>
                        <td><?php echo $value->productName;?></td>
                        <td> <?php echo \App\Format\Format::formatDate($value->date) ;?></td>
                        <?php
                            foreach ($shipingorder as $status){
                            if ($status->status=='shifted'){?>
                            <td>
                                <?php echo \App\Format\Format::formatDate($status->update_time) ;?>
                            </td>
                        <?php }} ?>
                        <td><a href="viewdetails.php?id=<?php echo $value->cmrId;?>&Date=<?php echo $value->date?>">View Details</a></td>
                        <td>
                            <?php
                            //                                if ($value->status=='shifted'){?>
                            <!--                                    <a href="?cmrid=--><?php //echo $id?><!--&price=--><?php //echo $value->price?><!--&time=--><?php //echo $value->date?><!--">Shifted</a>-->
                            <!--                               --><?php //}else{
                            echo $value->status;//}
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($value->status=='shifted'){?>
                                <a href="?cmrid=<?php echo $id?>&time=<?php echo $value->date?>">Shifted</a>
                            <?php } elseif ($value->status=='confirm'){?>
                                <a onclick="return confirm('Are You Sure To Delete???')" href="?delproid=<?php echo $value->cmrId?>&price=<?php echo $value->price?>&time=<?php echo $value->date?>">X</a>
                            <?php } else{
                                echo "N/A";
                            }?>
                        </td>

                    </tr>
                    <?php $serial++; } ?>


            </table>




        </div>


    </div>
</div>

<?php include_once 'inc/footer.php';?>
