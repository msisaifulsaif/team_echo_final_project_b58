<?php
require_once ('../../../vendor/autoload.php');
use App\Classes\Cart;
use App\Model\Session;
\App\Model\Session::init();

$login=\App\Model\Session::get("cmrlogin");
if ($login==false){
    \App\Utility\Utility::redirect('login.php');
}


$obj=new Cart();
$allData= $obj->index();

if ($_SERVER["REQUEST_METHOD"]=="POST"){
    $quantity=$_POST['quantity'];
    $obj->setData($_POST);
    $obj->update();
    if ($quantity<=0){
        $obj->delete();
    }
    $msg=Message::message();
}

if (!isset($_GET['id'])){
    echo "<meta http-equiv='refresh' content='0;URL=?id=teamecho?id' />";
}

?>
<?php include_once('header.php'); ?>

 <div class="main">
    <div class="content">
    	<div class="cartoption">		
			<div class="cartpage">
			    	<h2>Your Cart</h2>
                <?php if(isset($msg)) echo "<div id='message'>$msg</div>";?>
						<table class="tblone">
							<tr>
                                <th width="5%">No</th>
								<th width="20%">Product Name</th>
								<th width="10%">Image</th>
								<th width="15%">Price</th>
								<th width="25%">Quantity</th>
								<th width="15%">Total Price</th>
								<th width="10%">Action</th>
							</tr>
							<?php

                            $serial=1;
                            $sum=0;
                            $qty=0;
                            foreach ($allData as $value) {?>

                            <tr>
                                <td><?php echo $serial;?></td>
								<td><?php echo $value->product_name;?></td>
								<td><img src="admin/img/<?php echo $value->image;?>" alt=""/></td>
								<td>Tk. <?php echo $value->price;?></td>
								<td>
									<form action="" method="post">
                                        <input type="hidden" name="cartid" value="<?php echo $value->cartid;?>"/>
                                        <input type="number" name="quantity" value="<?php echo $value->quantity;?>"/>
										<input type="submit" name="submit" value="Update"/>
									</form>
								</td>
								<td><?php
                                    echo $total=$value->price*$value->quantity;

                                    ?> </td>
								<td><a onclick="return confirm('Are You Sure To Delete???')" href="deletecart.php?cartid=<?php echo $value->cartid;?>">X</a></td>

                                <?php
                                $sum=$sum+$total;
                                $qty=$qty+$value->quantity;
                                \App\Model\Session::set("qty",$qty);
                                ?>

							</tr>
							<?php $serial++; } ?>

							
						</table>
            <?php
                if ($allData){
            ?>

						<table style="float:right;text-align:left;" width="40%">
							<tr>
								<th>Sub Total : </th>
								<td>TK. <?php echo $sum?></td>
							</tr>
							<tr>
								<th>VAT : </th>
								<td>10%</td>
							</tr>
							<tr>
								<th>Grand Total :</th>
								<td>TK.
                                <?php echo $total=$sum*0.1+$sum;
								\App\Model\Session::set("total",$total);
								//\App\Utility\Utility::dd(\App\Model\Session::get("total"));
								?> </td>
							</tr>
					   </table>
            <?php } else{
                    \App\Utility\Utility::redirect("index.php");
                }
            ?>

					</div>
					<div class="shopping">
						<div class="shopleft">
							<a href="index.php"> <img src="images/shop.png" alt="" /></a>
						</div>
						<div class="shopright">
							<a href="payment.php"> <img src="images/check.png" alt="" /></a>
						</div>
					</div>
    	</div>  	
       <div class="clear"></div>
    </div>
 </div>
</div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>
  <?php include_once('inc/footer.php'); ?>