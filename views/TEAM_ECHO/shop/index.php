<?php include_once('header.php'); ?>
<?php
require_once ('../../../vendor/autoload.php');

$obj=new \App\Classes\Product();
$newBrandProduct= $obj->getProductByBrand();
$featured =$obj->getFeaturedProduct();
$newProduct=$obj->getNewProduct();
$allProduct=$obj->getAllProduct();





######################## pagination code block#1 of 2 start ######################################
$recordCount= count($featured);



if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;


if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;



$pages = ceil($recordCount/$itemsPerPage);
$someData = $obj->indexPaginator($page,$itemsPerPage);
$featured= $someData;


$serial = (  ($page-1) * $itemsPerPage ) +1;



if($serial<1) $serial=1;
####################### pagination code block#1 of 2 end #########################################

















?>

<style>
    .more{margin-top: 120px;}
</style>

	<div class="header_bottom">
		<div class="header_bottom_left">
			<div class="section group">
                <?php
                foreach ($newBrandProduct as $value){?>
				<div class="listview_1_of_2 images_1_of_2">
					<div class="listimg listimg_2_of_1">
                        <a href="preview.php?id=<?php echo $value->id;?>"><img src="admin/img/<?php echo $value->image?>" height="100px" width="100px" alt="" /></a>
                    </div>
					<div class="text list_2_of_1">
						  <h2><?php echo $value->product_name;?></h2>
                        <p><?php

                            $text = $value->description;
                            if (strlen($text)>40) {
                                $text=substr($text,0,40 );
                                echo $text;
                            }else{
                                echo $text;
                            }
                            ?>
                        </p>
						  <div class="button"><span><a href="preview.php?id=<?php echo $value->id;?>">Add to cart</a></span></div>
                        <p style="font-size: 15px; color: #0A246A"><?php echo $value->count?> views</p>
                    </div>
				</div>

                <?php } ?>
                <div class="text list_2_of_1">
                    <div class="more"><a class="btn btn-success" href="topbrands.php">See More Brand..</a>
                    </div>
                </div>

			</div>


		  <div class="clear"></div>
		</div>
			 <div class="header_bottom_right_images">
		   <!-- FlexSlider -->
             
			<section class="slider">
				  <div class="flexslider">
					<ul class="slides">
						<li><img src="images/1.jpg" alt=""/></li>
						<li><img src="images/2.jpg" alt=""/></li>
						<li><img src="images/3.jpg" alt=""/></li>
						<li><img src="images/4.jpg" alt=""/></li>
				    </ul>
				  </div>
	      </section>
<!-- FlexSlider -->
	    </div>
	  <div class="clear"></div>
  </div>	

 <div class="main">
    <div class="content">
    	<div class="content_top">
    		<div class="heading">
    		<h3>Feature Products</h3>
    		</div>
    		<div class="clear"></div>
    	</div>
        <div class="section group">

            <?php
            foreach ($featured as $value){?>
                <div class="grid_1_of_4 images_1_of_4">


                    <a href="preview.php?id=<?php echo $value->id;?>"><img src="admin/img/<?php echo $value->image?>" height="100px" width="100px" alt="" /></a>
                    <h2><?php echo $value->product_name;?> </h2>
                    <p><?php

                        $text = $value->description;
                        if (strlen($text)>40) {
                            $text=substr($text,0,40 );
                            echo $text;
                        }else{
                            echo $text;
                        }
                        ?>
                    </p>
                    <p><span class="price">$<?php echo $value->price;?></span></p>
                    <div class="button"><span><a href="preview.php?id=<?php echo $value->id;?>" class="details">Details</a></span></div>
                    <p style="font-size: 15px; color: #0A246A"><?php echo $value->count?> views</p>

                </div>
            <?php } ?>

        </div>


        <!--  ######################## pagination code block#2 of 2 start ###################################### -->
        <div align="left" class="container">
            <ul class="pagination">

                <?php

                $pageMinusOne  = $page-1;
                $pagePlusOne  = $page+1;


                if($page>$pages) App\Utility\Utility::redirect('index.php?Page=$pages');

                if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "&laquo" . "</a></li>";


                for($i=1;$i<=$pages;$i++)
                {
                    if($i==$page) echo '<li class="active"><a href=""> '. $i . '</a></li>';
                    else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                }
                if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "&raquo" . "</a></li>";

                ?>

<!--                <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >-->
<!--                    --><?php
//                    if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
//                    else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';
//
//                    if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
//                    else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';
//
//                    if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
//                    else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';
//
//                    if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
//                    else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';
//
//                    if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
//                    else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';
//
//                    if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
//                    else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
//                    ?>
<!--                </select>-->
            </ul>
        </div>
        <!--  ######################## pagination code block#2 of 2 end ###################################### -->


			<div class="content_bottom">
    		<div class="heading">
    		<h3>New Products</h3>
    		</div>
    		<div class="clear"></div>
    	</div>
			<div class="section group">
                <?php
                foreach ($newProduct as $value){?>
                    <div class="grid_1_of_4 images_1_of_4">


                        <a href="preview.php?id=<?php echo $value->id;?>"><img src="admin/img/<?php echo $value->image?>" height="100px" width="100px" alt="" /></a>
                        <h2><?php echo $value->product_name;?> </h2>
                        <p><?php

                            $text = $value->description;
                            if (strlen($text)>40) {
                                $text=substr($text,0,40 );
                                echo $text;
                            }else{
                                echo $text;
                            }
                            ?>
                        </p>
                        <p><span class="price">$<?php echo $value->price;?></span></p>
                        <div class="button"><span><a href="preview.php?id=<?php echo $value->id;?>" class="details">Details</a></span></div>
                        <p style="font-size: 15px; color: #0A246A"><?php echo $value->count?> views</p>

                    </div>
                <?php } ?>


			</div>


        <?php



        ?>

        <?php

        ######################## pagination code block#1 of 2 start ######################################
        $recordCount= count($allProduct);



        if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
        else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
        else   $page = 1;
        $_SESSION['Page']= $page;


        if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
        else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
        else   $itemsPerPage = 3;
        $_SESSION['ItemsPerPage']= $itemsPerPage;



        $pages = ceil($recordCount/$itemsPerPage);
        $someData = $obj->indexPaginator($page,$itemsPerPage);
        $featured= $someData;


        $serial = (  ($page-1) * $itemsPerPage ) +1;



        if($serial<1) $serial=1;
        ####################### pagination code block#1 of 2 end #########################################


        ?>






        <div class="content_top">
            <div class="heading">
                <h3>All Products</h3>
            </div>
            <div class="clear"></div>
        </div>
        <div class="section group">

            <?php
            foreach ($featured as $value){?>
                <div class="grid_1_of_4 images_1_of_4">


                    <a href="preview.php?id=<?php echo $value->id;?>"><img src="admin/img/<?php echo $value->image?>" height="100px" width="100px" alt="" /></a>
                    <h2><?php echo $value->product_name;?> </h2>
                    <p><?php

                        $text = $value->description;
                        if (strlen($text)>40) {
                            $text=substr($text,0,40 );
                            echo $text;
                        }else{
                            echo $text;
                        }
                        ?>
                    </p>
                    <p><span class="price">$<?php echo $value->price;?></span></p>
                    <div class="button"><span><a href="preview.php?id=<?php echo $value->id;?>" class="details">Details</a></span></div>
                    <p style="font-size: 15px; color: #0A246A"><?php echo $value->count?> views</p>

                </div>
            <?php } ?>

        </div>


        <!--  ######################## pagination code block#2 of 2 start ###################################### -->
        <div align="left" class="container">
            <ul class="pagination">

                <?php

                $pageMinusOne  = $page-1;
                $pagePlusOne  = $page+1;


                if($page>$pages) App\Utility\Utility::redirect('index.php?Page=$pages');

                if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "&laquo" . "</a></li>";


                for($i=1;$i<=$pages;$i++)
                {
                    if($i==$page) echo '<li class="active"><a href=""> '. $i . '</a></li>';
                    else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                }
                if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "&raquo" . "</a></li>";

                ?>

                <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                    <?php
                    if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                    else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                    if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                    else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                    if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                    else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                    if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                    else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                    if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                    else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                    if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                    else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                    ?>
                </select>
            </ul>
        </div>
        <!--  ######################## pagination code block#2 of 2 end ###################################### -->












    </div>
 </div>
</div>

<?php include_once('inc/footer.php'); ?>
  
