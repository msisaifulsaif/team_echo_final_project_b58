<?php include_once('header.php'); ?>

<div class="row panel-body">
    <?php
    $obj=new \App\Classes\Brand();
    $allData= $obj->index();
    ?>
<?php foreach ($allData as $value) {?>
    <div class="col-md-2 btn btn-info"><a href="productbybrand.php?brandId=<?php echo $value->id?>"><span style="font-size: 18px"><?php echo $value->brand?></span></a></div>
<?php } ?>

</div>

 <div class="main">
    <div class="content">
    	<div class="content_top">
    		<div class="heading">
                <h3>I-Phone</h3>
    		</div>
    		<div class="clear"></div>
    	</div>
	      <div class="section group">

              <?php
              $obj=new \App\Classes\Product();
              $brandIphone= $obj->getByIphone();
              if (empty($brandIphone)){
                  echo "<h2 class='alert alert-info'> This Brand Not Available Now</h2>";
              }
              ?>

              <?php
              foreach ($brandIphone as $value){?>
                  <div class="grid_1_of_4 images_1_of_4">


                      <a href="preview.php?id=<?php echo $value->id;?>"><img src="admin/img/<?php echo $value->image?>" height="100px" width="100px" alt="" /></a>
                      <h2><?php echo $value->product_name;?> </h2>
                      <p><?php

                          $text = $value->description;
                          if (strlen($text)>40) {
                              $text=substr($text,0,40 );
                              echo $text;
                          }else{
                              echo $text;
                          }
                          ?>
                      </p>
                      <p><span class="price">$<?php echo $value->price;?></span></p>
                      <div class="button"><span><a href="preview.php?id=<?php echo $value->id;?>" class="details">Details</a></span></div>

                  </div>
              <?php } ?>



			</div>
		<div class="content_bottom">
    		<div class="heading">
    		<h3>Samsung</h3>
    		</div>
    		<div class="clear"></div>
    	</div>

        <div class="section group">

            <?php
            $obj=new \App\Classes\Product();
            $brandSamsung= $obj->getBySamsung();
            if (empty($brandSamsung)){
                echo "<h2 class='alert alert-info'> This Brand Not Available Now</h2>";
            }
            ?>

            <?php
            foreach ($brandSamsung as $value){?>
                <div class="grid_1_of_4 images_1_of_4">


                    <a href="preview.php?id=<?php echo $value->id;?>"><img src="admin/img/<?php echo $value->image?>" height="100px" width="100px" alt="" /></a>
                    <h2><?php echo $value->product_name;?> </h2>
                    <p><?php

                        $text = $value->description;
                        if (strlen($text)>40) {
                            $text=substr($text,0,40 );
                            echo $text;
                        }else{
                            echo $text;
                        }
                        ?>
                    </p>
                    <p><span class="price">$<?php echo $value->price;?></span></p>
                    <div class="button"><span><a href="preview.php?id=<?php echo $value->id;?>" class="details">Details</a></span></div>

                </div>
            <?php } ?>



        </div>

	    <div class="content_bottom">
    		<div class="heading">
    		<h3>Dell</h3>
    		</div>
    		<div class="clear"></div>
    	</div>

        <div class="section group">

            <?php
            $obj=new \App\Classes\Product();
            $brandDell= $obj->getByDell();
            if (empty($brandDell)){
                echo "<h2 class='alert alert-info'> This Brand Not Available Now</h2>";
            }
            ?>

            <?php
            foreach ($brandDell as $value){?>
                <div class="grid_1_of_4 images_1_of_4">


                    <a href="preview.php?id=<?php echo $value->id;?>"><img src="admin/img/<?php echo $value->image?>" height="100px" width="100px" alt="" /></a>
                    <h2><?php echo $value->product_name;?> </h2>
                    <p><?php

                        $text = $value->description;
                        if (strlen($text)>40) {
                            $text=substr($text,0,40 );
                            echo $text;
                        }else{
                            echo $text;
                        }
                        ?>
                    </p>
                    <p><span class="price">$<?php echo $value->price;?></span></p>
                    <div class="button"><span><a href="preview.php?id=<?php echo $value->id;?>" class="details">Details</a></span></div>

                </div>
            <?php } ?>



        </div>



    </div>
 </div>
</div>
   <?php include_once('inc/footer.php'); ?>

