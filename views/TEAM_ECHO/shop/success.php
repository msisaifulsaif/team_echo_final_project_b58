<?php include_once 'header.php';?>

<?php
$login=\App\Model\Session::get("cmrlogin");
if ($login==false){
    \App\Utility\Utility::redirect('login.php');
}
?>

<style>
    .psuccess{width: 500px;min-height: 200px;text-align: center;border: 1px solid #d1d1d1;margin: 0 auto;padding: 50px;}
    .psuccess h2{border-bottom:1px solid #d1d1d1; margin-bottom: 20px; padding-bottom: 10px; }
    .psuccess p{line-height: 25px;font-size: 18px;text-align: left;}
</style>

<div class="main">
    <div class="content">
        <div class="section group">
            <div class="psuccess">
                <h2>Success</h2>

                <?php
                $objCart=new \App\Classes\Cart();
                $id=\App\Model\Session::get('cmrId');
                $amount=$objCart->payableAmount($id);

                if ($amount){
                    $sum=0;
                    foreach ($amount as $result){
                        $price=$result->price;
                        $sum=$price+$sum;
                    }


                }

                ?>

                <p>Payment Successful</p>
                <p>Total Payable Amount (Including Vat):<?php echo$total=$sum+ $sum*0.1;?></p>
                <p>Thanks for Purchase.Receive Your Order Successfully.We will contact you As Soon As Possible with delivery details.Here is Your order details....
                <a href="orderdetails.php">Visit Here..</a></p>

            </div>

        </div>

    </div>
</div>

<?php include_once 'inc/footer.php';?>
