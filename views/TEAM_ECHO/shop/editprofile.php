<?php include_once 'header.php';?>

<?php
use App\Model\Session;
$obj=new \App\Classes\User();
$login=\App\Model\Session::get("cmrlogin");
if ($login==false){
    \App\Utility\Utility::redirect('login.php');
}
if ($_SERVER["REQUEST_METHOD"]=="POST"){
    $obj->setData($_POST);
    $obj->update();
}

echo $msg=\App\Message\Message::message();

?>

<style>
    .tblone{width: 550px; margin: 0px auto;border: 2px solid #ddd}
    .tblone tr td{text-align: justify}
    .tblone input[type="text"]{width: 400px; padding: 5px; font-size: 15px;  }
</style>

<div class="main">
    <div class="content">
        <div class="section group">
            <?php
            $id=Session::get('cmrId');
            $objCmr=new \App\Classes\User();
            $cmrData= $objCmr->cmrInfo($id);
            //\App\Utility\Utility::dd($cmrData);
            ?>

<form action="" method="post">
    <table class="tblone">
        <tr>
            <td colspan="2"><h2>Update Profile Details</h2></td>
        </tr>
        <tr>
            <td width="20%">Name</td>
            <td><input type="text" name="name" value="<?php echo $cmrData->name?>"></td>
        </tr>
        <tr>
            <td>Address</td>
            <td><input type="text" name="address" value="<?php echo $cmrData->address?>"></td>
        </tr>
        <tr>
            <td>City</td>
            <td><input type="text" name="city" value="<?php echo $cmrData->city?>"></td>
        </tr>
        <tr>
            <td>Country</td>
            <td><input type="text" name="country" value="<?php echo $cmrData->country?>"></td>
        </tr><tr>
            <td>Zip-Code</td>
            <td><input type="text" name="zip" value="<?php echo $cmrData->zip?>"></td>
        </tr>
        <tr>
            <td>Phone</td>
            <td><input type="text" name="phone" value="<?php echo $cmrData->phone?>"></td>
        </tr>
        <input type="hidden" name="id" value="<?php echo $cmrData->id?>">
        <tr>
            <td></td>
            <td><input type="submit" name="submit" value="Update"></td>
        </tr>

    </table>

</form>

        </div>
        <div class="clear"></div>
    </div>
</div>

<?php include_once 'inc/footer.php';?>
