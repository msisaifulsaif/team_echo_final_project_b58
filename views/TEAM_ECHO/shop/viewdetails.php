<?php include_once 'header.php';?>
<?php
require_once ('../../../vendor/autoload.php');
use App\Classes\Cart;
use App\Model\Session;
\App\Model\Session::init();

$login=\App\Model\Session::get("cmrlogin");
if ($login==false){
    \App\Utility\Utility::redirect('login.php');
}


$objCart=new \App\Classes\Cart();
$id=\App\Model\Session::get('cmrId');
$multipleData=$objCart->viewByMultipleData($_GET['id'],$_GET['Date']);



if (!isset($_GET['id'])){
    echo "<meta http-equiv='refresh' content='0;URL=?id=teamecho?id' />";
}
?>



<div class="main">
    <div class="content">
        <div class="section group">

                <h2>Your Cart</h2>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td colspan="8"><h2>Order List</h2></td>
                    </tr>
                    <tr>
                        <th>Sl</th>
                        <th>Pro Name</th>
                        <th>Quantity</th>
                        <th>price</th>
                        <th>total price</th>
                        <th>date</th>
                        <th>image</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $i=1;
                    $sum=0;
                    foreach ($multipleData as $value){?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $value->productName;?></td>
                            <td><?php echo $value->quantity;?></td>
                            <td><?php echo $value->price/$value->quantity;?></td>
                            <td><?php echo $value->price;?></td>
                            <td><?php echo \App\Format\Format::formatDate($value->date) ;?></td>
                            <td><img src="admin/img/<?php echo $value->image;?>" height="100px" width="100px"></td>

                        </tr>
                        <?php $i++; $sum=$sum+$value->price;}?>
                    <tr>
                        <td></td><td></td><td></td><td></td><td></td>
                        <td colspan="5">Total=<?php echo $sum;?></td>
                        <td></td><td></td>
                    </tr>
                    </tbody>


                </table>


            </div>


    </div>
</div>

<?php include_once 'inc/footer.php';?>
