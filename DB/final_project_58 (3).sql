-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2017 at 04:38 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final_project_58`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL DEFAULT 'admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `user`, `email`, `password`, `level`) VALUES
(1, 'Saiful', 'saiful1994', 'admin@gmail.com', '202cb962ac59075b964b07152d234b70', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brand`
--

CREATE TABLE `tbl_brand` (
  `id` int(11) NOT NULL,
  `brand` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_brand`
--

INSERT INTO `tbl_brand` (`id`, `brand`) VALUES
(8, 'I-phone'),
(9, 'Samsung'),
(10, 'Acer'),
(11, 'Dell'),
(12, 'PC jewllery'),
(13, 'Tanisque Jewllery'),
(14, 'Naksatra Jewllery'),
(15, 'Nirvana Jwellery'),
(16, 'Arong'),
(17, 'Shoilpik'),
(18, 'Le reve'),
(19, 'CatsEye'),
(20, 'Splash'),
(21, 'Ghucci'),
(22, 'Berbery'),
(23, 'Coach'),
(24, 'Cristian Dior'),
(25, 'Nick'),
(26, 'Adidas'),
(27, 'Sperry'),
(28, 'Convers'),
(29, 'Vans'),
(30, 'Miu Miu'),
(31, 'Stuart Weitzman'),
(32, 'Brian Atwood'),
(33, 'Alexander McQueen'),
(34, 'Jimmy Choo'),
(35, 'Cristian Louboutin'),
(38, 'DKNY'),
(39, 'TechnoMarine'),
(40, 'Technos'),
(41, 'Tianin'),
(42, 'Timex'),
(43, 'Veens'),
(44, 'TAG'),
(45, 'Seth thomas'),
(46, 'Logines'),
(47, 'Rotary'),
(48, 'Tissort'),
(49, 'Oakley'),
(50, 'POLO'),
(51, 'Ray ban'),
(52, 'Ralph'),
(53, 'Berbery persol'),
(54, 'LG'),
(55, 'Panasonic'),
(56, 'Macro software'),
(57, 'King Software'),
(58, 'Microsoft Sotware'),
(59, 'Jet Brains'),
(60, 'Lenevo');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `cartid` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`cartid`, `session_id`, `product_id`, `product_name`, `price`, `quantity`, `image`) VALUES
(1, '4abkqe0nj4e95ji5u7238cq6l2', 25, 'Accessories', 360.00, 1, '1498294177new-pic2.jpg'),
(10, 'nm2nr1v4f5kgr7nfsr8q4r5779', 24, 'Laptop', 2500.00, 1, '1498294136preview-img.jpg'),
(14, 'hnk1qr2a25pfj9nm7scbkutt3r', 25, 'Accessories', 360.00, 1, '1498294177new-pic2.jpg'),
(16, 'hg9kh29rdhetcguvkvf6a72l3a', 25, 'Accessories', 360.00, 1, '1498294177new-pic2.jpg'),
(17, 'hg9kh29rdhetcguvkvf6a72l3a', 24, 'Laptop', 2500.00, 1, '1498294136preview-img.jpg'),
(30, 'vp8994mqj77cm85l5f65tee984', 25, 'Accessories', 360.00, 1, '1498294177new-pic2.jpg'),
(31, 'vp8994mqj77cm85l5f65tee984', 24, 'Laptop', 2500.00, 1, '1498294136preview-img.jpg'),
(32, 'vp8994mqj77cm85l5f65tee984', 23, 'DSLR', 360.00, 1, '1498294082preview-img6.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category`) VALUES
(6, 'Mobile'),
(7, 'Desktop'),
(8, 'Laptop'),
(9, 'Appliances'),
(10, 'Software'),
(11, 'Jewellery'),
(12, 'Cloth'),
(13, 'Walet'),
(14, 'Ladies Purse'),
(16, 'Gents Shoes'),
(17, 'Ladies shoes'),
(24, 'Ladies Watch'),
(25, 'Gents Watch'),
(26, 'Ladies Sunglasses'),
(27, 'Gents Sunglasses');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_compare`
--

CREATE TABLE `tbl_compare` (
  `id` int(11) NOT NULL,
  `cmrId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_confirm`
--

CREATE TABLE `tbl_confirm` (
  `id` int(11) NOT NULL,
  `productId` varchar(255) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `cmrId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_confirm`
--

INSERT INTO `tbl_confirm` (`id`, `productId`, `productName`, `cmrId`, `date`, `status`, `update_time`) VALUES
(50, '100,98', 'Learning Software,Office Software', 35, '2017-07-07 21:05:19', 'confirm', '2017-07-07 21:07:05'),
(51, '106', 'Mobile', 35, '2017-07-07 21:08:07', 'confirm', '2017-07-07 21:12:13'),
(52, '100', 'Learning Software', 35, '2017-07-07 21:12:22', 'confirm', '2017-07-07 21:25:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `name`, `email`, `phone`, `subject`, `date`, `status`) VALUES
(1, 'dsf', 'msisaifulsaif@gmail.com', '01772037067', 'sfdsdfsdfjifshuvhudj ', '2017-07-12 04:05:24', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL,
  `cmrId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(100) NOT NULL DEFAULT 'pending',
  `is_order` varchar(255) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `cmrId`, `productId`, `productName`, `quantity`, `price`, `image`, `date`, `status`, `is_order`) VALUES
(94, 35, 100, 'Learning Software', 2, 520.00, '1499169006php strom.jpg', '2017-07-07 21:05:19', 'pending', 'Yes'),
(95, 35, 98, 'Office Software', 3, 450.00, '1499168899King soft Office.JPG', '2017-07-07 21:05:19', 'pending', 'Yes'),
(96, 35, 106, 'Mobile', 1, 25000.00, '1499169981lenovo-smartphone-p2-series.png', '2017-07-07 21:08:07', 'pending', 'Yes'),
(97, 35, 100, 'Learning Software', 1, 260.00, '1499169006php strom.jpg', '2017-07-07 21:12:22', 'pending', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `catid` int(11) NOT NULL,
  `brandid` int(11) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `price` float(10,3) NOT NULL,
  `image` varchar(100) NOT NULL,
  `featured` int(11) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `product_name`, `catid`, `brandid`, `description`, `price`, `image`, `featured`, `count`) VALUES
(21, 'I phone 7', 6, 8, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum', 900.000, '1498293968pic4.png', 1, 3),
(22, 'Desktop', 7, 9, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum', 600.000, '1498294016pic3.jpg', 1, 18),
(23, 'DSLR', 9, 9, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum', 360.000, '1498294082preview-img6.jpg', 1, 20),
(24, 'Laptop', 8, 11, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum', 2500.000, '1498294136preview-img.jpg', 1, 2),
(25, 'Web Cam', 9, 10, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum', 360.000, '1498294177new-pic2.jpg', 2, 7),
(26, 'Fridge', 9, 10, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum', 3600.000, '1498294204pic3.png', 2, 3),
(27, 'Ring', 11, 15, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 3000.000, '1499155630images (15).jpg', 2, 1),
(28, 'Ear Ring', 11, 14, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 3900.000, '1499155961images (46).jpg', 2, 1),
(29, 'Bangle', 11, 15, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 4500.000, '1499156002images (47).jpg', 2, 0),
(30, 'Nacklace', 11, 12, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 3900.000, '1499156035images (17).jpg', 2, 0),
(31, 'Ear Ring', 11, 12, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 2800.000, '1499156165images (18).jpg', 2, 0),
(32, 'Ear Ring', 11, 14, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 4300.000, '1499156260e.jpg', 2, 0),
(33, 'Nacklace', 11, 13, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 4000.000, '1499156288tanisiq2.jpg', 2, 0),
(34, 'Nacklace', 11, 13, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 4900.000, '1499156327tanisq.jpg', 2, 0),
(35, 'Cloth', 12, 16, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 2300.000, '1499157225a.jpg', 2, 0),
(36, 'Cloth', 12, 19, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1200.000, '1499157247b.jpg', 2, 0),
(37, 'Cloth', 12, 18, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 3299.000, '1499157270c.jpg', 2, 0),
(38, 'Cloth', 12, 17, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 2300.000, '14991573026.jpg', 2, 0),
(39, 'Cloth', 12, 18, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 2100.000, '1499157331d2.JPG', 2, 0),
(40, 'Cloth', 12, 16, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 3200.000, '1499157363d1.JPG', 2, 0),
(41, 'Cloth', 12, 19, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 3200.000, '1499157443kid.jpg', 2, 0),
(42, 'Cloth', 12, 20, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1400.000, '1499157501k1.jpg', 2, 0),
(43, 'Jewellery', 11, 15, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 2100.000, '1499159177images (13).jpg', 2, 0),
(44, 'walet', 13, 21, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 650.000, '1499159342download (1).jpg', 2, 0),
(45, 'Ladies purses', 14, 22, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 350.000, '1499159688images (28).jpg', 2, 0),
(46, 'Ladies purses', 14, 24, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 420.000, '1499159780images (38).jpg', 2, 0),
(47, 'Ladies purses', 14, 23, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 220.000, '1499159816images (34).jpg', 2, 0),
(48, 'Ladies purses', 14, 23, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 450.000, '1499159856DUDINI-New-Fashion-Women-font-b-Wallet-b-font-Cartoon-font-b-Animation-b-font-Small.jpg', 2, 0),
(49, 'Ladies purses', 14, 22, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 300.000, '1499159976images (31).jpg', 2, 0),
(50, 'Ladies purses', 14, 22, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 420.000, '1499159949images (32).jpg', 2, 0),
(51, 'Gents shoes', 16, 25, '', 1050.000, '1499160758download.jpg', 2, 0),
(52, 'Gents shoes', 16, 26, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1500.000, '1499160833images (18).jpg', 2, 0),
(53, 'Gents shoes', 16, 25, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1200.000, '1499161049small_1406614882gents_shoes_b_1.jpg', 2, 1),
(54, 'Gents shoes', 16, 23, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1800.000, '1499160890images (15).jpg', 2, 0),
(55, 'Gents shoes', 16, 26, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 850.000, '1499160918images (10).jpg', 2, 0),
(56, 'Gents shoes', 16, 25, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 990.000, '1499160940images (11).jpg', 2, 0),
(57, 'Gents shoes', 16, 20, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 650.000, '1499160977images (12).jpg', 2, 0),
(58, 'Gents shoes', 16, 26, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1020.000, '1499161015images (17).jpg', 2, 0),
(59, 'Ladies shoes', 16, 24, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1600.000, '1499161351images (22).jpg', 2, 0),
(60, 'Ladies shoes', 17, 35, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 2250.000, '1499161529images (23).jpg', 2, 0),
(61, 'Ladies shoes', 16, 30, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1250.000, '1499161559images (22).jpg', 2, 0),
(62, 'Ladies shoes', 17, 34, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1750.000, '1499161597images (21).jpg', 2, 0),
(63, 'Ladies shoes', 17, 31, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 750.000, '1499161637images (28).jpg', 2, 0),
(64, 'Ladies shoes', 17, 33, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 2150.000, '1499161685images (26).jpg', 2, 0),
(65, 'Ladies shoes', 17, 32, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1100.000, '1499161723images (24).jpg', 2, 0),
(66, 'Ladies Watch', 24, 0, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 0.000, '1499162019DKNY1.jpg', 2, 0),
(67, 'Ladies Watch', 24, 38, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 850.000, '1499162354DKNY.jpg', 2, 0);
INSERT INTO `tbl_product` (`id`, `product_name`, `catid`, `brandid`, `description`, `price`, `image`, `featured`, `count`) VALUES
(68, 'Ladies Watch', 24, 44, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1200.000, '1499162415TAG.jpg', 2, 0),
(69, 'Ladies Watch', 24, 45, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1300.000, '1499162468Seth Thomas..jpg', 2, 0),
(70, 'Ladies Watch', 24, 41, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1650.000, '1499162498Tianjin.jpg', 2, 0),
(71, 'Ladies Watch', 24, 40, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 2000.000, '1499162705Technos..jpg', 2, 0),
(72, 'Ladies Watch', 24, 42, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 1400.000, '1499162625Timex .jpg', 2, 0),
(73, 'Ladies Watch', 24, 39, '<p>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem</p>', 850.000, '1499162678TechnoMarine.jpg', 2, 0),
(74, 'Washing Machine', 9, 55, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 65000.000, '1499164995images (29).jpg', 2, 0),
(75, 'Fridge', 9, 54, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 50000.000, '1499165037images.jpg', 2, 0),
(76, 'Iron', 9, 55, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 4500.000, '1499165081images (1).jpg', 2, 0),
(77, 'Air Conditioner', 9, 54, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 120000.000, '1499165124images (2).jpg', 2, 0),
(78, 'Gents Watch', 25, 42, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 980.000, '1499165576images (7).jpg', 2, 0),
(79, 'Gents Watch', 25, 48, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1200.000, '1499165614images (10).jpg', 2, 0),
(80, 'Gents Watch', 25, 53, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 658.000, '1499165692images (8).jpg', 2, 0),
(81, 'Gents Watch', 25, 44, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 890.000, '1499165731images (10).jpg', 2, 0),
(82, 'Gents Watch', 25, 24, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1100.000, '1499165828images (9).jpg', 2, 0),
(83, 'Gents Sunglass', 27, 21, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1100.000, '1499166269images (17).jpg', 2, 0),
(84, 'Gents Sunglass', 27, 22, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1500.000, '1499166313images (18).jpg', 2, 0),
(85, 'Gents Sunglass', 27, 50, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 2500.000, '1499166355images (19).jpg', 2, 0),
(86, 'Gents Sunglass', 27, 51, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1500.000, '1499166400images (24).jpg', 2, 0),
(87, 'Gents Sunglass', 27, 49, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1600.000, '1499166432images (21).jpg', 2, 0),
(88, 'Gents Watch', 27, 52, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 8900.000, '1499166477images (27).jpg', 2, 0),
(89, 'Ladies Sunglass', 26, 21, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1200.000, '1499166552download (1).jpg', 2, 0),
(90, 'Ladies Sunglass', 26, 51, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1500.000, '1499166586download (3).jpg', 2, 0),
(91, 'Ladies Sunglass', 26, 49, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 2500.000, '1499166638download (4).jpg', 2, 0),
(92, 'Ladies Sunglass', 26, 50, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1800.000, '1499166680images (12).jpg', 2, 0),
(93, 'Ladies Sunglass', 26, 50, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 980.000, '1499166717images (15).jpg', 2, 0),
(94, 'Ladies Sunglass', 26, 52, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 1230.000, '1499166769images (12).jpg', 2, 0),
(95, 'Gaming Software', 10, 56, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 250.000, '1499168775Brust fire Marco game.jpg', 2, 0),
(96, 'Gaming Software', 10, 0, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 230.000, '1499168802World War craft  game.jpg', 2, 0),
(97, 'Office Software', 10, 58, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 150.000, '1499168860Home Office.jpg', 2, 0),
(98, 'Office Software', 10, 57, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 150.000, '1499168899King soft Office.JPG', 2, 25),
(99, 'Learning Software', 10, 58, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 260.000, '1499168957Learn to Speak English.jpg', 2, 0),
(100, 'Learning Software', 10, 59, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 260.000, '1499169006php strom.jpg', 2, 89),
(101, 'Learning Software', 10, 0, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 150.000, '1499169043Html Editor.jpg', 2, 0),
(102, 'Learning Software', 10, 0, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 240.000, '1499169084Anime Studio 9.jpg', 2, 0),
(103, 'Mobile', 6, 8, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 90000.000, '1499169820images.jpg', 2, 0),
(104, 'Mobile', 6, 9, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 35000.000, '1499169860download (1).jpg', 2, 0),
(105, 'Mobile', 6, 9, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 36000.000, '1499169939download.jpg', 2, 0),
(106, 'Mobile', 6, 60, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 25000.000, '1499169981lenovo-smartphone-p2-series.png', 2, 23),
(107, 'Dell', 7, 11, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 38000.000, '1499170076images (4).jpg', 2, 0),
(108, 'Dell', 7, 11, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 65000.000, '1499170106714g0cUhESL._SL1500_.jpg', 2, 0),
(109, 'Lenevo', 7, 60, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 98000.000, '1499170157images (8).jpg', 2, 0),
(110, 'Laptop', 8, 60, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 38000.000, '1499170387images (7).jpg', 2, 0),
(111, 'Laptop', 8, 11, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 45000.000, '1499170444dell.jpg', 2, 2),
(112, 'Laptop', 8, 9, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 56000.000, '1499170510images (3).jpg', 2, 2),
(113, 'Laptop', 8, 60, '<p><span>Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem Ipsum&nbsp;Lorem', 58000.000, '1499170611images (5).jpg', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `zip` int(11) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email_token` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'active',
  `trxid` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `address`, `city`, `country`, `zip`, `phone`, `email`, `password`, `email_token`, `status`, `trxid`) VALUES
(1, 'Saiful', 'Andrkilla', 'Chittagong', 'Bangladesh', 4000, '11223344', 'msi@gmail.com', '202cb962ac59075b964b07152d234b70', '', 'active', '0'),
(19, 'Saiful', 'sa', 'chittagong', 'Bangladesh', 4000, '1122331', 'msisaifulsaif@gmail.com', '202cb962ac59075b964b07152d234b70', 'Yes', 'disable', '0'),
(20, 'Saiful', 'andrkilla', 'chittagong', 'Bangladesh', 4000, '112233', 'teamecho@gmail.com', '202cb962ac59075b964b07152d234b70', 'd83b0e5dfe813b2140eee45240754b14', 'disable', '0'),
(34, 'Saiful', 'andrkilla', 'chittagong', 'Bangladesh', 4000, '112233', 'msisaifulsaif1@gmail.com', '202cb962ac59075b964b07152d234b70', 'Yes', 'active', '0'),
(35, 'Saiful', 'andrkilla', '', '', 0, '', 'admin@gmail.com', '202cb962ac59075b964b07152d234b70', 'ad1775d339ec06510ed5d62d7b8ca22e', 'active', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wlist`
--

CREATE TABLE `tbl_wlist` (
  `id` int(11) NOT NULL,
  `cmrId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`cartid`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_compare`
--
ALTER TABLE `tbl_compare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_confirm`
--
ALTER TABLE `tbl_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_wlist`
--
ALTER TABLE `tbl_wlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `cartid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tbl_compare`
--
ALTER TABLE `tbl_compare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_confirm`
--
ALTER TABLE `tbl_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_wlist`
--
ALTER TABLE `tbl_wlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
